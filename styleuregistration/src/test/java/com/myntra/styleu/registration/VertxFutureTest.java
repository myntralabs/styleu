package com.myntra.styleu.registration;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.ws.AsyncHandler;

@RunWith(VertxUnitRunner.class)
public class VertxFutureTest {

    @Test
    public void testCase1(TestContext testContext)
    {
        Async async = testContext.async();
        Future future = Future.future();
        future.setHandler(x->{
            if (((AsyncResult)x).succeeded()) {
                System.out.println("suceeded");
            }
            else
            {
                System.out.println("failed");
            }
            async.complete();
        });
        TestResponse1 response1 = new TestResponse1();
        Future doTask1Future = Future.future();
      //  Future doTask2Future = Future.future();
        this.doTask1(response1,doTask1Future);
        doTask1Future.compose(x->{
            doTask2((TestResponse2) x,future.completer());
            //return doTask2Future;
        },future);
        async.await();
    }

    private void doTask1(TestResponse1 object,Handler<AsyncResult<TestResponse2>> resultHandler)
    {
        System.out.println("doing task 1");
       resultHandler.handle(Future.failedFuture("task1 failed"));//Future.succeededFuture(new TestResponse2()));
        //resultHandler.handle(Future.succeededFuture(new TestResponse2()));
    }

    private void doTask2(TestResponse2 object, Handler<AsyncResult<Void>> resultHandler)
    {
        System.out.println("doing task 2");
        resultHandler.handle(Future.succeededFuture());
    }

    public class TestResponse1 {
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private String name;
    }

    public class TestResponse2 {
        public String getName2() {
            return name2;
        }

        public void setName2(String name2) {
            this.name2 = name2;
        }

        private String name2;
    }
}
