package com.myntra.styleu.registration;

import io.vertx.core.json.Json;
import org.junit.Test;

public class PojoToJson  {
    @Test
    public void test()
    {
        TestClass testObj = new TestClass();
        testObj.setName("hello");
        String encode = Json.encode(testObj);

    }

    public class TestClass{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private String name;
    }

}
