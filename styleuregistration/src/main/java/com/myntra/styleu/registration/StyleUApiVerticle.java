package com.myntra.styleu.registration;

import io.vertx.core.Future;
import myntra.vertx.common.RestAPIVerticle;

public class StyleUApiVerticle extends RestAPIVerticle {
    public static final String SERVICE_NAME = "styleu-registration-api";
    private static final String API_ANALYZE = "/register";

    @Override
    public void start(Future<Void> future) throws Exception
    {
        super.start();

    }

}
