package com.myntra.styleu.registration;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.serviceproxy.ProxyHelper;
import myntra.vertx.common.BaseMicroserviceVerticle;

/**
 * Hello world!
 *
 */
public class StyleURegistrationVerticle extends BaseMicroserviceVerticle
{
    @Override
    public void start(Future<Void> future) throws Exception {
        super.start();
        this.deployRestApiVerticle().setHandler(future.completer());
    }

    public Future<Void> deployRestApiVerticle()
    {
        Future<String> future = Future.future();
        vertx.deployVerticle(new StyleUApiVerticle(),
                new DeploymentOptions().setConfig(config()),
                future.completer());
        return future.map(r -> null);

    }


}
