package com.myntra.style.styleanalysis;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceReference;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import io.vertx.core.http.HttpClient;

import java.io.Console;

import static org.junit.Assert.*;

@RunWith(VertxUnitRunner.class)
public class StyleAnalysisVerticleTest {
    private Vertx vertx;

    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
    }


    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void testrestapiServiceDiscovery(TestContext testContext) throws InterruptedException {
        Async async = testContext.async();
        ServiceDiscovery serviceDiscovery = ServiceDiscovery.create(vertx);
        serviceDiscovery.getRecord(r->r.getName().equals("styleanalysis-rest-api"),ar->{
            if (ar.succeeded())
            {
               if (ar.result()!=null)
               {
                 System.out.println(ar.result().getName());
                 System.out.println(ar.result().getLocation());
                 System.out.println(ar.result().getMetadata());
                 System.out.println(ar.result().getStatus());
                 ServiceReference reference = serviceDiscovery.getReference(ar.result());
                 HttpClient as = reference.getAs(HttpClient.class);
                 as.request(HttpMethod.POST,"/analyze",x->{
                     System.out.println(x.statusCode());
                 });
                 reference.release();
               }
               else {

               }
            } else {

            }
        });

        Thread.sleep(10);
        async.complete();

    }

    @Test
    public void testebServiceDiscoverty()
    {

    }

}