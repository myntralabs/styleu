package com.myntra.style.styleanalysis;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FaceApiResponseTest {
//    private Vertx vertx;
//    @Before
//    public void deployStyleAnalysisVerticle(TestContext testContext)
//    {
//        vertx = Vertx.vertx();
//        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
//    }
//
//
//    @After
//    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
//        System.out.println("After test");
//        System.out.print(java.time.Instant.now());
//        vertx.close(testContext.asyncAssertSuccess());
//    }

    @Test
    public void testFaceApiResponse()
    {
        String s = "[{\"faceId\":\"88150cbd-0804-43c0-924a-54837b59018b\",\"faceRectangle\":{\"top\":151,\"left\":497,\"width\":209,\"height\":209},\"faceAttributes\":{\"gender\":\"male\",\"age\":23.2}}]";
        String s1 ="{\"faceId\":\"ba5b0c4b-58ca-4475-aba1-408ec0c8c993\",\"faceRectangle\":{\"top\":167,\"left\":324,\"width\":189,\"height\":189}}";
        JsonArray jsonArray = new JsonArray(s);
        FaceApiResponse apiResponse = new FaceApiResponse(jsonArray);
        Assert.assertEquals(apiResponse.getGender(),"male");
    }

    @Test
    public void testParseFaceApiResponse()
    {
        String s= "[{\"faceId\":\"88150cbd-0804-43c0-924a-54837b59018b\",\"faceRectangle\":{\"top\":151,\"left\":497,\"width\":209,\"height\":209},\"faceAttributes\":{\"gender\":\"male\",\"age\":23.2}}]";
        JsonArray array = new JsonArray(s);
        for(int i=0;i<array.size();i++)
        {
            JsonObject jsonObject = array.getJsonObject(i);
            System.out.println();
        }

        FaceApiResponse response = new FaceApiResponse(array);

//        JsonObject object= new JsonObject(s);
    }
}
