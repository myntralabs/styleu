package com.myntra.style.styleanalysis.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.myntra.style.styleanalysis.LooksApiFacade;
import com.myntra.style.styleanalysis.LooksApiRequest;
import com.myntra.style.styleanalysis.StyleAnalysis;
import com.myntra.style.styleanalysis.StyleAnalysisVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

@RunWith(VertxUnitRunner.class)
public class LooksApiFacadeImplTest {
    private Vertx vertx;
    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
    }

    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void analyze(TestContext testContext) throws Exception {
        Async async = testContext.async();
        LooksApiFacade looksApiFacade = new LooksApiFacadeImpl(vertx,new JsonObject());
        java.util.List<String> s = new java.util.LinkedList<>();
        s.add("151091722");

        LooksApiRequest request = new LooksApiRequest(new JsonObject());
        request.setTopwear(s);
        request.setBottomwear(s);
        looksApiFacade.analyze(request, x->{
            if (x.succeeded())
            {
                Assert.assertTrue(true);
                System.out.println(x.result().toJson().toString());
            }
            else
            {
                Assert.assertTrue(false);
            }
            async.complete();
        });

        async.await();
    }


    @Test
    public void testCreateRequestTest(TestContext testContext)
    {
        LooksApiFacadeImpl impl = new LooksApiFacadeImpl(vertx,new JsonObject());
        java.util.List<String> s = new java.util.LinkedList<>();
        s.add("151091722");
        s.add("1510917");
        impl.convertToApiRequest(s);
    }

    @Test
    public void testConstructLooksApiRequest(TestContext testContext)
    {
//        LooksApiFacadeImpl impl = new LooksApiFacadeImpl(vertx,new JsonObject());
//        java.util.List<String> s = new java.util.LinkedList<>();
//        StyleAnalysis styleAnalysis = new StyleAnalysis();
//        String analysisJsonStr = "{\n" +
//                "        \"analysis\": [\n" +
//                "            {\n" +
//                "                \"category_score\": 76,\n" +
//                "                \"colour\": \"#222221\",\n" +
//                "                \"bounding_box\": [\n" +
//                "                    167,\n" +
//                "                    386,\n" +
//                "                    926,\n" +
//                "                    1417\n" +
//                "                ],\n" +
//                "                \"similar_styles\": [\n" +
//                "                    \"1919140\",\n" +
//                "                    \"1669884\",\n" +
//                "                    \"1375773\",\n" +
//                "                    \"2038806\",\n" +
//                "                    \"1500502\",\n" +
//                "                    \"1669886\",\n" +
//                "                    \"2075463\"\n" +
//                "                ],\n" +
//                "                \"attribute_list\": [\n" +
//                "                    \"PRINTED\",\n" +
//                "                    \"PULLOVER\",\n" +
//                "                    \"LONG SLEEVES\",\n" +
//                "                    \"ROUND NECK\"\n" +
//                "                ],\n" +
//                "                \"category\": \"TSHIRT\",\n" +
//                "                \"recommendation\": {\n" +
//                "                    \"reco_type\": \"not_available\"\n" +
//                "                }\n" +
//                "            }\n" +
//                "        ],\n" +
//                "        \"scores\": {\n" +
//                "            \"overall_score\": 86\n" +
//                "        }\n" +
//                "    }";
//
//        JsonObject analysis = new JsonObject(analysisJsonStr);
//        styleAnalysis.setAnalysisJson(analysis);
//        LooksApiRequest request = impl.convertToApiRequest(styleAnalysis);
//        HashMap<String, ArrayList<String>> categoryRecommendation = request.getCategoryRecommendation();
//        Assert.assertTrue(categoryRecommendation.get("TSHIRT").size()==6);
    }




}