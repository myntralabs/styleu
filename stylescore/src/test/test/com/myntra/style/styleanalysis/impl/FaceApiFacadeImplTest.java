package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.FaceApiRequest;
import com.myntra.style.styleanalysis.StyleAnalysisVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.junit.Assert.*;

@RunWith(VertxUnitRunner.class)
public class FaceApiFacadeImplTest {
    private Vertx vertx;
    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
    }


    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void analyze(TestContext testContext) throws Exception {
        Async async = testContext.async();
        FaceApiFacadeImpl faceApiFacadeImpl = new FaceApiFacadeImpl(vertx, new JsonObject());
        JsonObject request = new JsonObject();
        FaceApiRequest apiRequest = new FaceApiRequest(request);
        ClassLoader classLoader = getClass().getClassLoader();
        byte[] plainImgByte = IOUtils.toByteArray(classLoader.getResourceAsStream("img3.jpg"));
        byte[] encode = Base64.getEncoder().encode(plainImgByte);
        apiRequest.setImage(new String(encode));
        faceApiFacadeImpl.analyze(apiRequest,x->{
            if(x.succeeded())
            {
                System.out.println(x.result().toJson());
                System.out.println("succeeded");
                Assert.assertTrue(true);
            }
            else
            {
                System.out.println(x.cause());
                System.out.println("failed");
                Assert.assertTrue(false);
            }
            async.complete();
        }

        );
        async.await();
    }

}