package com.myntra.style.styleanalysis.impl.rules;

import com.myntra.style.styleanalysis.StyleAnalysisVerticle;
import com.myntra.style.styleanalysis.rules.BadgeRule;
import com.myntra.style.styleanalysis.rules.BadgeRuleExchange;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import myntra.vertx.common.data.JdbcRepositoryWrapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class BadgeRuleTest {

    private Vertx vertx;
    private JdbcRepositoryWrapper jdbcRepositoryWrapper;
    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
        vertx = Vertx.vertx();
        JsonObject config = new JsonObject()
                .put("url", "jdbc:mysql://localhost:3306/styleu" )
                .put("driver_class", "com.mysql.jdbc.Driver")
                .put("max_pool_size", 3)
                .put("user","root")
                .put("password","password");
        jdbcRepositoryWrapper = new JdbcRepositoryWrapper(vertx,config);

    }


    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
        //  jdbcRepositoryWrapper.close();
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void testBadgeRule(TestContext testContext)
    {
        Async async = testContext.async();
        BadgeRuleExchange exchange = new BadgeRuleExchange();
        exchange.setScore(76);
        BadgeRule rule = new BadgeRule(jdbcRepositoryWrapper);
        rule.apply(exchange,x->{
            if (x.succeeded())
            {
                System.out.println("Succeeded");
                System.out.println(Json.encode(x.result()));
            }
            else
            {
                System.out.println(x.cause());
                System.out.println("failed");
            }
            async.complete();
        });
        async.await();
    }
}
