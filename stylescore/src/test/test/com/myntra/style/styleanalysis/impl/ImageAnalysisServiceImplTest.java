package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.ImageAnalysisService;
import com.myntra.style.styleanalysis.StyleAnalysis;
import com.myntra.style.styleanalysis.StyleAnalysisVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Base64;

import static org.junit.Assert.*;

@RunWith(VertxUnitRunner.class)
public class ImageAnalysisServiceImplTest {

    private Vertx vertx;
    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
    }


    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void testAnalyze(TestContext testContext) throws Exception {
        Async async = testContext.async();
        JsonObject jsonObject = new JsonObject();
        JsonObject config = jsonObject;
        ImageAnalysisService service = new ImageAnalysisServiceImpl(vertx,config);
        StyleAnalysis analysis = new StyleAnalysis(jsonObject);
        ClassLoader classLoader = getClass().getClassLoader();
        byte[] plainImgByte = IOUtils.toByteArray(classLoader.getResourceAsStream("img.jpg"));
        byte[] encode = Base64.getEncoder().encode(plainImgByte);
        analysis.setEncodeImage(new String(encode));
        service.analyze(analysis,x->{
            if (x.succeeded())
            {
                //System.out.println(x.result());
                Assert.assertTrue(true);
            }
            else
            {
                System.out.println("failed");
               // System.out.println(x.cause());
                Assert.assertFalse(false);
            }
            async.complete();
        });

        async.await();
    }


    @Test
    public void testParseJson(TestContext context)
    {
      //  String s ="5"
    }

}