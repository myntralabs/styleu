package com.myntra.style.styleanalysis.impl.rules;
import com.myntra.style.styleanalysis.ImageAnalysisService;
import com.myntra.style.styleanalysis.StyleAnalysisVerticle;
import com.myntra.style.styleanalysis.rules.ImageAnalysisExchange;
import com.myntra.style.styleanalysis.rules.ImageAnalysisRuleImpl;
import com.myntra.style.styleanalysis.rules.Rule;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class ImageAnalysisRulesApplicationTest {
    private Vertx vertx;
    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
    }


    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void constructionTest()
    {
        ImageAnalysisRuleImpl rule = new ImageAnalysisRuleImpl(vertx,new JsonObject());
        Assert.assertNotNull(rule);
    }

    @Test
    public void testApplyRule(TestContext testContext)
    {
        Async async = testContext.async();
        ImageAnalysisRuleImpl rule = new ImageAnalysisRuleImpl(vertx,new JsonObject());
        ImageAnalysisExchange imageAnalysisExchange = new ImageAnalysisExchange();
        imageAnalysisExchange.setScore(75.0);
        rule.apply(imageAnalysisExchange, x->{
            if (x.succeeded())
            {
                System.out.println("succeeded ");
                //System.out.println(x.result().getOneline().getOnelinercategory());
                Assert.assertTrue(true);

            }
            else
            {
                System.out.println(x.cause());
                Assert.assertTrue(false);
            }
            async.complete();
        });

        async.await();

    }

}
