package com.myntra.style.styleanalysis.impl.rules;

import com.myntra.style.styleanalysis.StyleAnalysisVerticle;
import com.myntra.style.styleanalysis.rules.OnelinerExchange;
import com.myntra.style.styleanalysis.rules.OnelinerOutput;
import com.myntra.style.styleanalysis.rules.OnelinerRule;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import myntra.vertx.common.data.JdbcRepositoryWrapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class OnelinerRuleTest {
    private Vertx vertx;
    private JdbcRepositoryWrapper jdbcRepositoryWrapper;
    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
        vertx = Vertx.vertx();
        JsonObject config = new JsonObject()
                .put("url", "jdbc:mysql://localhost:3306/styleu" )
                .put("driver_class", "com.mysql.jdbc.Driver")
                .put("max_pool_size", 3)
                .put("user","root")
                .put("password","password");
        jdbcRepositoryWrapper = new JdbcRepositoryWrapper(vertx,config);

    }


    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
      //  jdbcRepositoryWrapper.close();
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void constructionTest(TestContext testContext)
    {
        OnelinerRule onelinerRule = new OnelinerRule(jdbcRepositoryWrapper);
        Assert.assertNotNull(onelinerRule);
    }

    @Test
    public void applyRuleTest(TestContext testContext) throws InterruptedException {
        Async async = testContext.async();
        OnelinerRule onelinerRule = new OnelinerRule(jdbcRepositoryWrapper);
        OnelinerExchange exchange = new OnelinerExchange();
        exchange.setDeviceid("abcd");
        exchange.setScore(75);
        onelinerRule.apply(exchange, y-> {
            if (y.succeeded())
            {
                OnelinerOutput result = y.result();
                System.out.println(result.getOnelinercategory());
                System.out.println(result.getOneLinerText());
                //Assert.assertTrue(result.getOnelinercategory().equals("Neutral"));
            }
            else {
                Assert.assertTrue(false);
            }

            async.complete();
        });

        async.await();
    }
}
