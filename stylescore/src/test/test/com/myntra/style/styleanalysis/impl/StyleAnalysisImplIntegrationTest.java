package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.FaceApiRequest;
import com.myntra.style.styleanalysis.StyleAnalysis;
import com.myntra.style.styleanalysis.StyleAnalysisVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Base64;

import static org.junit.Assert.*;

@RunWith(VertxUnitRunner.class)
public class StyleAnalysisImplIntegrationTest {
    private Vertx vertx;

    @Before
    public void deployStyleAnalysisVerticle(TestContext testContext)
    {
        vertx = Vertx.vertx();
        vertx.deployVerticle(StyleAnalysisVerticle.class.getCanonicalName(),testContext.asyncAssertSuccess());
    }

    @After
    public void undeployStyleAnalysisVerticle(TestContext testContext)  {
        System.out.println("After test");
        System.out.print(java.time.Instant.now());
        vertx.close(testContext.asyncAssertSuccess());
    }

    @Test
    public void analyseTest(TestContext testContext) throws Exception {
        Async async = testContext.async();
        StyleAnalysisImpl impl = new StyleAnalysisImpl(vertx,new BadgeRuleServiceImpl(vertx,new JsonObject()), new FaceApiFacadeImpl(vertx,new JsonObject()),new JsonObject());
        JsonObject request = new JsonObject();
        FaceApiRequest apiRequest = new FaceApiRequest(request);
        ClassLoader classLoader = getClass().getClassLoader();
        byte[] plainImgByte = IOUtils.toByteArray(classLoader.getResourceAsStream("kurtas.jpg"));
        byte[] encode = Base64.getEncoder().encode(plainImgByte);
        apiRequest.setImage(new String(encode));
        StyleAnalysis analysis = new StyleAnalysis();
        analysis.setEncodeImage(apiRequest.getImage());
        impl.analyse(analysis, x->{
            if(x.succeeded())
            {
                try {
                    System.out.println("s");
                    System.out.println(Json.encode(x.result()));
                    Assert.assertTrue(true);
                }catch (Exception ex)
                {
                    System.out.println("exception message :" + ex.getMessage());
                }
            }
            else
            {
                Assert.assertFalse(true);
            }
            async.complete();
        });
        async.await();
    }
}