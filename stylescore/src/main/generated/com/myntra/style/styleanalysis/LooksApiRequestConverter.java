/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.myntra.style.styleanalysis;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.myntra.style.styleanalysis.LooksApiRequest}.
 *
 * NOTE: This class has been automatically generated from the {@link com.myntra.style.styleanalysis.LooksApiRequest} original class using Vert.x codegen.
 */
public class LooksApiRequestConverter {

  public static void fromJson(JsonObject json, LooksApiRequest obj) {
    if (json.getValue("bottomwear") instanceof JsonArray) {
      java.util.ArrayList<java.lang.String> list = new java.util.ArrayList<>();
      json.getJsonArray("bottomwear").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setBottomwear(list);
    }
    if (json.getValue("toCategorys") instanceof JsonObject) {
      json.getJsonObject("toCategorys").forEach(entry -> {
        if (entry.getValue() instanceof String)
          obj.addToCategory(entry.getKey(), (String)entry.getValue());
      });
    }
    if (json.getValue("topwear") instanceof JsonArray) {
      java.util.ArrayList<java.lang.String> list = new java.util.ArrayList<>();
      json.getJsonArray("topwear").forEach( item -> {
        if (item instanceof String)
          list.add((String)item);
      });
      obj.setTopwear(list);
    }
  }

  public static void toJson(LooksApiRequest obj, JsonObject json) {
    if (obj.getBottomwear() != null) {
      JsonArray array = new JsonArray();
      obj.getBottomwear().forEach(item -> array.add(item));
      json.put("bottomwear", array);
    }
    if (obj.getTopwear() != null) {
      JsonArray array = new JsonArray();
      obj.getTopwear().forEach(item -> array.add(item));
      json.put("topwear", array);
    }
  }
}