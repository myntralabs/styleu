/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.myntra.style.styleanalysis.rxjava;

import java.util.Map;
import rx.Observable;
import rx.Single;
import com.myntra.style.styleanalysis.FaceApiRequest;
import io.vertx.rxjava.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.AsyncResult;
import com.myntra.style.styleanalysis.FaceApiResponse;
import io.vertx.core.Handler;


@io.vertx.lang.rxjava.RxGen(com.myntra.style.styleanalysis.FaceApiFacade.class)
public class FaceApiFacade {

  public static final io.vertx.lang.rxjava.TypeArg<FaceApiFacade> __TYPE_ARG = new io.vertx.lang.rxjava.TypeArg<>(
    obj -> new FaceApiFacade((com.myntra.style.styleanalysis.FaceApiFacade) obj),
    FaceApiFacade::getDelegate
  );

  private final com.myntra.style.styleanalysis.FaceApiFacade delegate;
  
  public FaceApiFacade(com.myntra.style.styleanalysis.FaceApiFacade delegate) {
    this.delegate = delegate;
  }

  public com.myntra.style.styleanalysis.FaceApiFacade getDelegate() {
    return delegate;
  }

  public void analyze(FaceApiRequest apiRequest, Handler<AsyncResult<FaceApiResponse>> handler) { 
    delegate.analyze(apiRequest, handler);
  }

  public Single<FaceApiResponse> rxAnalyze(FaceApiRequest apiRequest) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      analyze(apiRequest, fut);
    }));
  }

  public static FaceApiFacade create(Vertx vertx, JsonObject config) { 
    FaceApiFacade ret = FaceApiFacade.newInstance(com.myntra.style.styleanalysis.FaceApiFacade.create(vertx.getDelegate(), config));
    return ret;
  }

  public static FaceApiFacade createProxy(Vertx vertx, String address) { 
    FaceApiFacade ret = FaceApiFacade.newInstance(com.myntra.style.styleanalysis.FaceApiFacade.createProxy(vertx.getDelegate(), address));
    return ret;
  }


  public static FaceApiFacade newInstance(com.myntra.style.styleanalysis.FaceApiFacade arg) {
    return arg != null ? new FaceApiFacade(arg) : null;
  }
}
