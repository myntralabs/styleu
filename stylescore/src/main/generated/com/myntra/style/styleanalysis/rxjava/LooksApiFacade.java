/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.myntra.style.styleanalysis.rxjava;

import java.util.Map;
import rx.Observable;
import rx.Single;
import com.myntra.style.styleanalysis.LooksApiRequest;
import io.vertx.rxjava.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.AsyncResult;
import com.myntra.style.styleanalysis.LooksApiResponse;
import io.vertx.core.Handler;


@io.vertx.lang.rxjava.RxGen(com.myntra.style.styleanalysis.LooksApiFacade.class)
public class LooksApiFacade {

  public static final io.vertx.lang.rxjava.TypeArg<LooksApiFacade> __TYPE_ARG = new io.vertx.lang.rxjava.TypeArg<>(
    obj -> new LooksApiFacade((com.myntra.style.styleanalysis.LooksApiFacade) obj),
    LooksApiFacade::getDelegate
  );

  private final com.myntra.style.styleanalysis.LooksApiFacade delegate;
  
  public LooksApiFacade(com.myntra.style.styleanalysis.LooksApiFacade delegate) {
    this.delegate = delegate;
  }

  public com.myntra.style.styleanalysis.LooksApiFacade getDelegate() {
    return delegate;
  }

  public void analyze(LooksApiRequest request, Handler<AsyncResult<LooksApiResponse>> resultHandler) { 
    delegate.analyze(request, resultHandler);
  }

  public Single<LooksApiResponse> rxAnalyze(LooksApiRequest request) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      analyze(request, fut);
    }));
  }

  public static LooksApiFacade create(Vertx vertx, JsonObject config) { 
    LooksApiFacade ret = LooksApiFacade.newInstance(com.myntra.style.styleanalysis.LooksApiFacade.create(vertx.getDelegate(), config));
    return ret;
  }

  public static LooksApiFacade createProxy(Vertx vertx, String address) { 
    LooksApiFacade ret = LooksApiFacade.newInstance(com.myntra.style.styleanalysis.LooksApiFacade.createProxy(vertx.getDelegate(), address));
    return ret;
  }


  public static LooksApiFacade newInstance(com.myntra.style.styleanalysis.LooksApiFacade arg) {
    return arg != null ? new LooksApiFacade(arg) : null;
  }
}
