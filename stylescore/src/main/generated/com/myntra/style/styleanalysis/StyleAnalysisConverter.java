/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.myntra.style.styleanalysis;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.myntra.style.styleanalysis.StyleAnalysis}.
 *
 * NOTE: This class has been automatically generated from the {@link com.myntra.style.styleanalysis.StyleAnalysis} original class using Vert.x codegen.
 */
public class StyleAnalysisConverter {

  public static void fromJson(JsonObject json, StyleAnalysis obj) {
    if (json.getValue("analysisJson") instanceof JsonObject) {
      obj.setAnalysisJson(((JsonObject)json.getValue("analysisJson")).copy());
    }
    if (json.getValue("deviceId") instanceof String) {
      obj.setDeviceId((String)json.getValue("deviceId"));
    }
    if (json.getValue("encodeImage") instanceof String) {
      obj.setEncodeImage((String)json.getValue("encodeImage"));
    }
    if (json.getValue("faceAttribute") instanceof JsonObject) {
      obj.setFaceAttribute(((JsonObject)json.getValue("faceAttribute")).copy());
    }
    if (json.getValue("faceRectangle") instanceof JsonObject) {
      obj.setFaceRectangle(((JsonObject)json.getValue("faceRectangle")).copy());
    }
    if (json.getValue("gender") instanceof String) {
      obj.setGender((String)json.getValue("gender"));
    }
    if (json.getValue("looks") instanceof JsonObject) {
      obj.setLooks(((JsonObject)json.getValue("looks")).copy());
    }
    if (json.getValue("scoreAnalysis") instanceof JsonObject) {
      obj.setScoreAnalysis(((JsonObject)json.getValue("scoreAnalysis")).copy());
    }
  }

  public static void toJson(StyleAnalysis obj, JsonObject json) {
    if (obj.getAnalysisJson() != null) {
      json.put("analysisJson", obj.getAnalysisJson());
    }
    if (obj.getDeviceId() != null) {
      json.put("deviceId", obj.getDeviceId());
    }
    if (obj.getEncodeImage() != null) {
      json.put("encodeImage", obj.getEncodeImage());
    }
    if (obj.getFaceAttribute() != null) {
      json.put("faceAttribute", obj.getFaceAttribute());
    }
    if (obj.getFaceRectangle() != null) {
      json.put("faceRectangle", obj.getFaceRectangle());
    }
    if (obj.getGender() != null) {
      json.put("gender", obj.getGender());
    }
    if (obj.getLooks() != null) {
      json.put("looks", obj.getLooks());
    }
    if (obj.getScoreAnalysis() != null) {
      json.put("scoreAnalysis", obj.getScoreAnalysis());
    }
  }
}