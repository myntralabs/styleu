/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.myntra.style.styleanalysis;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

/**
 * Converter for {@link com.myntra.style.styleanalysis.Badge}.
 *
 * NOTE: This class has been automatically generated from the {@link com.myntra.style.styleanalysis.Badge} original class using Vert.x codegen.
 */
public class BadgeConverter {

  public static void fromJson(JsonObject json, Badge obj) {
    if (json.getValue("badge") instanceof String) {
      obj.setBadge((String)json.getValue("badge"));
    }
    if (json.getValue("badgeDescription") instanceof String) {
      obj.setBadgeDescription((String)json.getValue("badgeDescription"));
    }
    if (json.getValue("badgeId") instanceof String) {
      obj.setBadgeId((String)json.getValue("badgeId"));
    }
    if (json.getValue("badgeName") instanceof String) {
      obj.setBadgeName((String)json.getValue("badgeName"));
    }
    if (json.getValue("badgeText") instanceof String) {
      obj.setBadgeText((String)json.getValue("badgeText"));
    }
    if (json.getValue("category") instanceof String) {
      obj.setCategory((String)json.getValue("category"));
    }
    if (json.getValue("uri") instanceof String) {
      obj.setUri((String)json.getValue("uri"));
    }
    if (json.getValue("uriH") instanceof String) {
      obj.setUriH((String)json.getValue("uriH"));
    }
  }

  public static void toJson(Badge obj, JsonObject json) {
    if (obj.getBadge() != null) {
      json.put("badge", obj.getBadge());
    }
    if (obj.getBadgeDescription() != null) {
      json.put("badgeDescription", obj.getBadgeDescription());
    }
    if (obj.getBadgeId() != null) {
      json.put("badgeId", obj.getBadgeId());
    }
    if (obj.getBadgeName() != null) {
      json.put("badgeName", obj.getBadgeName());
    }
    if (obj.getBadgeText() != null) {
      json.put("badgeText", obj.getBadgeText());
    }
    if (obj.getCategory() != null) {
      json.put("category", obj.getCategory());
    }
    if (obj.getUri() != null) {
      json.put("uri", obj.getUri());
    }
    if (obj.getUriH() != null) {
      json.put("uriH", obj.getUriH());
    }
  }
}