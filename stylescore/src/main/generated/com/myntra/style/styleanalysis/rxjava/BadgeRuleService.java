/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.myntra.style.styleanalysis.rxjava;

import java.util.Map;
import rx.Observable;
import rx.Single;
import com.myntra.style.styleanalysis.BadgeRuleEvaluationEntity;
import io.vertx.rxjava.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.AsyncResult;
import com.myntra.style.styleanalysis.Badge;
import io.vertx.core.Handler;


@io.vertx.lang.rxjava.RxGen(com.myntra.style.styleanalysis.BadgeRuleService.class)
public class BadgeRuleService {

  public static final io.vertx.lang.rxjava.TypeArg<BadgeRuleService> __TYPE_ARG = new io.vertx.lang.rxjava.TypeArg<>(
    obj -> new BadgeRuleService((com.myntra.style.styleanalysis.BadgeRuleService) obj),
    BadgeRuleService::getDelegate
  );

  private final com.myntra.style.styleanalysis.BadgeRuleService delegate;
  
  public BadgeRuleService(com.myntra.style.styleanalysis.BadgeRuleService delegate) {
    this.delegate = delegate;
  }

  public com.myntra.style.styleanalysis.BadgeRuleService getDelegate() {
    return delegate;
  }

  public void evaluate(BadgeRuleEvaluationEntity ruleEvaluationEntity, Handler<AsyncResult<Badge>> resultHandler) { 
    delegate.evaluate(ruleEvaluationEntity, resultHandler);
  }

  public Single<Badge> rxEvaluate(BadgeRuleEvaluationEntity ruleEvaluationEntity) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      evaluate(ruleEvaluationEntity, fut);
    }));
  }

  public static BadgeRuleService create(Vertx vertx, JsonObject config) { 
    BadgeRuleService ret = BadgeRuleService.newInstance(com.myntra.style.styleanalysis.BadgeRuleService.create(vertx.getDelegate(), config));
    return ret;
  }

  public static BadgeRuleService createProxy(Vertx vertx, String address) { 
    BadgeRuleService ret = BadgeRuleService.newInstance(com.myntra.style.styleanalysis.BadgeRuleService.createProxy(vertx.getDelegate(), address));
    return ret;
  }


  public static BadgeRuleService newInstance(com.myntra.style.styleanalysis.BadgeRuleService arg) {
    return arg != null ? new BadgeRuleService(arg) : null;
  }
}
