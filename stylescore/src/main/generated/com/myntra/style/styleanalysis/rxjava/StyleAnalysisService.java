/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.myntra.style.styleanalysis.rxjava;

import java.util.Map;
import rx.Observable;
import rx.Single;
import io.vertx.rxjava.core.Vertx;
import com.myntra.style.styleanalysis.StyleAnalysis;
import io.vertx.core.json.JsonObject;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;


@io.vertx.lang.rxjava.RxGen(com.myntra.style.styleanalysis.StyleAnalysisService.class)
public class StyleAnalysisService {

  public static final io.vertx.lang.rxjava.TypeArg<StyleAnalysisService> __TYPE_ARG = new io.vertx.lang.rxjava.TypeArg<>(
    obj -> new StyleAnalysisService((com.myntra.style.styleanalysis.StyleAnalysisService) obj),
    StyleAnalysisService::getDelegate
  );

  private final com.myntra.style.styleanalysis.StyleAnalysisService delegate;
  
  public StyleAnalysisService(com.myntra.style.styleanalysis.StyleAnalysisService delegate) {
    this.delegate = delegate;
  }

  public com.myntra.style.styleanalysis.StyleAnalysisService getDelegate() {
    return delegate;
  }

  public StyleAnalysisService analyse(StyleAnalysis styleAnalysis, Handler<AsyncResult<StyleAnalysis>> resultHandler) { 
    delegate.analyse(styleAnalysis, resultHandler);
    return this;
  }

  public Single<StyleAnalysis> rxAnalyse(StyleAnalysis styleAnalysis) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      analyse(styleAnalysis, fut);
    }));
  }

  public StyleAnalysisService store(StyleAnalysis styleAnalysis, Handler<AsyncResult<StyleAnalysis>> resultHandler) { 
    delegate.store(styleAnalysis, resultHandler);
    return this;
  }

  public Single<StyleAnalysis> rxStore(StyleAnalysis styleAnalysis) { 
    return Single.create(new io.vertx.rx.java.SingleOnSubscribeAdapter<>(fut -> {
      store(styleAnalysis, fut);
    }));
  }

  public static StyleAnalysisService create(Vertx vertx, BadgeRuleService badgeRuleService, FaceApiFacade faceApiFacade, JsonObject config) { 
    StyleAnalysisService ret = StyleAnalysisService.newInstance(com.myntra.style.styleanalysis.StyleAnalysisService.create(vertx.getDelegate(), badgeRuleService.getDelegate(), faceApiFacade.getDelegate(), config));
    return ret;
  }

  public static StyleAnalysisService createProxy(Vertx vertx, String address) { 
    StyleAnalysisService ret = StyleAnalysisService.newInstance(com.myntra.style.styleanalysis.StyleAnalysisService.createProxy(vertx.getDelegate(), address));
    return ret;
  }


  public static StyleAnalysisService newInstance(com.myntra.style.styleanalysis.StyleAnalysisService arg) {
    return arg != null ? new StyleAnalysisService(arg) : null;
  }
}
