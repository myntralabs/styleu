/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/** @module style-analysis-js/style_analysis_service */
!function (factory) {
  if (typeof require === 'function' && typeof module !== 'undefined') {
    factory();
  } else if (typeof define === 'function' && define.amd) {
    // AMD loader
    define('style-analysis-js/style_analysis_service-proxy', [], factory);
  } else {
    // plain old include
    StyleAnalysisService = factory();
  }
}(function () {
  var BadgeRuleService = require('style-analysis-js/badge_rule_service-proxy');
  var FaceApiFacade = require('style-analysis-js/face_api_facade-proxy');

  /**
 @class
  */
  var StyleAnalysisService = function(eb, address) {

    var j_eb = eb;
    var j_address = address;
    var closed = false;
    var that = this;
    var convCharCollection = function(coll) {
      var ret = [];
      for (var i = 0;i < coll.length;i++) {
        ret.push(String.fromCharCode(coll[i]));
      }
      return ret;
    };

    /**

     @public
     @param styleAnalysis {Object} 
     @param resultHandler {function} 
     @return {StyleAnalysisService}
     */
    this.analyse = function(styleAnalysis, resultHandler) {
      var __args = arguments;
      if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
        if (closed) {
          throw new Error('Proxy is closed');
        }
        j_eb.send(j_address, {"styleAnalysis":__args[0]}, {"action":"analyse"}, function(err, result) { __args[1](err, result &&result.body); });
        return that;
      } else throw new TypeError('function invoked with invalid arguments');
    };

    /**

     @public
     @param styleAnalysis {Object} 
     @param resultHandler {function} 
     @return {StyleAnalysisService}
     */
    this.store = function(styleAnalysis, resultHandler) {
      var __args = arguments;
      if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
        if (closed) {
          throw new Error('Proxy is closed');
        }
        j_eb.send(j_address, {"styleAnalysis":__args[0]}, {"action":"store"}, function(err, result) { __args[1](err, result &&result.body); });
        return that;
      } else throw new TypeError('function invoked with invalid arguments');
    };

  };

  /**

   @memberof module:style-analysis-js/style_analysis_service
   @param vertx {Vertx} 
   @param badgeRuleService {BadgeRuleService} 
   @param faceApiFacade {FaceApiFacade} 
   @param config {Object} 
   @return {StyleAnalysisService}
   */
  StyleAnalysisService.create = function(vertx, badgeRuleService, faceApiFacade, config) {
    var __args = arguments;
    if (__args.length === 4 && typeof __args[0] === 'object' && __args[0]._jdel && typeof __args[1] === 'object' && __args[1]._jdel && typeof __args[2] === 'object' && __args[2]._jdel && (typeof __args[3] === 'object' && __args[3] != null)) {
      if (closed) {
        throw new Error('Proxy is closed');
      }
      j_eb.send(j_address, {"vertx":__args[0], "badgeRuleService":__args[1], "faceApiFacade":__args[2], "config":__args[3]}, {"action":"create"});
      return;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @memberof module:style-analysis-js/style_analysis_service
   @param vertx {Vertx} 
   @param address {string} 
   @return {StyleAnalysisService}
   */
  StyleAnalysisService.createProxy = function(vertx, address) {
    var __args = arguments;
    if (__args.length === 2 && typeof __args[0] === 'object' && __args[0]._jdel && typeof __args[1] === 'string') {
      if (closed) {
        throw new Error('Proxy is closed');
      }
      j_eb.send(j_address, {"vertx":__args[0], "address":__args[1]}, {"action":"createProxy"});
      return;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = StyleAnalysisService;
    } else {
      exports.StyleAnalysisService = StyleAnalysisService;
    }
  } else {
    return StyleAnalysisService;
  }
});