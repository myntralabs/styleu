/*
 * Copyright 2014 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/** @module style-analysis-js/style_analysis_service */
var utils = require('vertx-js/util/utils');
var Vertx = require('vertx-js/vertx');
var BadgeRuleService = require('style-analysis-js/badge_rule_service');
var FaceApiFacade = require('style-analysis-js/face_api_facade');

var io = Packages.io;
var JsonObject = io.vertx.core.json.JsonObject;
var JStyleAnalysisService = Java.type('com.myntra.style.styleanalysis.StyleAnalysisService');
var StyleAnalysis = Java.type('com.myntra.style.styleanalysis.StyleAnalysis');

/**
 @class
*/
var StyleAnalysisService = function(j_val) {

  var j_styleAnalysisService = j_val;
  var that = this;

  /**

   @public
   @param styleAnalysis {Object} 
   @param resultHandler {function} 
   @return {StyleAnalysisService}
   */
  this.analyse = function(styleAnalysis, resultHandler) {
    var __args = arguments;
    if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
      j_styleAnalysisService["analyse(com.myntra.style.styleanalysis.StyleAnalysis,io.vertx.core.Handler)"](styleAnalysis != null ? new StyleAnalysis(new JsonObject(Java.asJSONCompatible(styleAnalysis))) : null, function(ar) {
      if (ar.succeeded()) {
        resultHandler(utils.convReturnDataObject(ar.result()), null);
      } else {
        resultHandler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  /**

   @public
   @param styleAnalysis {Object} 
   @param resultHandler {function} 
   @return {StyleAnalysisService}
   */
  this.store = function(styleAnalysis, resultHandler) {
    var __args = arguments;
    if (__args.length === 2 && (typeof __args[0] === 'object' && __args[0] != null) && typeof __args[1] === 'function') {
      j_styleAnalysisService["store(com.myntra.style.styleanalysis.StyleAnalysis,io.vertx.core.Handler)"](styleAnalysis != null ? new StyleAnalysis(new JsonObject(Java.asJSONCompatible(styleAnalysis))) : null, function(ar) {
      if (ar.succeeded()) {
        resultHandler(utils.convReturnDataObject(ar.result()), null);
      } else {
        resultHandler(null, ar.cause());
      }
    });
      return that;
    } else throw new TypeError('function invoked with invalid arguments');
  };

  // A reference to the underlying Java delegate
  // NOTE! This is an internal API and must not be used in user code.
  // If you rely on this property your code is likely to break if we change it / remove it without warning.
  this._jdel = j_styleAnalysisService;
};

StyleAnalysisService._jclass = utils.getJavaClass("com.myntra.style.styleanalysis.StyleAnalysisService");
StyleAnalysisService._jtype = {
  accept: function(obj) {
    return StyleAnalysisService._jclass.isInstance(obj._jdel);
  },
  wrap: function(jdel) {
    var obj = Object.create(StyleAnalysisService.prototype, {});
    StyleAnalysisService.apply(obj, arguments);
    return obj;
  },
  unwrap: function(obj) {
    return obj._jdel;
  }
};
StyleAnalysisService._create = function(jdel) {
  var obj = Object.create(StyleAnalysisService.prototype, {});
  StyleAnalysisService.apply(obj, arguments);
  return obj;
}
/**

 @memberof module:style-analysis-js/style_analysis_service
 @param vertx {Vertx} 
 @param badgeRuleService {BadgeRuleService} 
 @param faceApiFacade {FaceApiFacade} 
 @param config {Object} 
 @return {StyleAnalysisService}
 */
StyleAnalysisService.create = function(vertx, badgeRuleService, faceApiFacade, config) {
  var __args = arguments;
  if (__args.length === 4 && typeof __args[0] === 'object' && __args[0]._jdel && typeof __args[1] === 'object' && __args[1]._jdel && typeof __args[2] === 'object' && __args[2]._jdel && (typeof __args[3] === 'object' && __args[3] != null)) {
    return utils.convReturnVertxGen(StyleAnalysisService, JStyleAnalysisService["create(io.vertx.core.Vertx,com.myntra.style.styleanalysis.BadgeRuleService,com.myntra.style.styleanalysis.FaceApiFacade,io.vertx.core.json.JsonObject)"](vertx._jdel, badgeRuleService._jdel, faceApiFacade._jdel, utils.convParamJsonObject(config)));
  } else throw new TypeError('function invoked with invalid arguments');
};

/**

 @memberof module:style-analysis-js/style_analysis_service
 @param vertx {Vertx} 
 @param address {string} 
 @return {StyleAnalysisService}
 */
StyleAnalysisService.createProxy = function(vertx, address) {
  var __args = arguments;
  if (__args.length === 2 && typeof __args[0] === 'object' && __args[0]._jdel && typeof __args[1] === 'string') {
    return utils.convReturnVertxGen(StyleAnalysisService, JStyleAnalysisService["createProxy(io.vertx.core.Vertx,java.lang.String)"](vertx._jdel, address));
  } else throw new TypeError('function invoked with invalid arguments');
};

module.exports = StyleAnalysisService;