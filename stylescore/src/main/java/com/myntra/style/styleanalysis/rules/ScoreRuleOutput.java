package com.myntra.style.styleanalysis.rules;

public class ScoreRuleOutput {
    public String getScoreAdjective() {
        return scoreAdjective;
    }

    public void setScoreAdjective(String scoreAdjective) {
        this.scoreAdjective = scoreAdjective;
    }

    private String scoreAdjective;

    private double percentile;

    public double getPercentile() {
        return percentile;
    }

    public void setPercentile(double percentile) {
        this.percentile = percentile;
    }
}
