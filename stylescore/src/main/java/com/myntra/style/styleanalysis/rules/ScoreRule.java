package com.myntra.style.styleanalysis.rules;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import myntra.vertx.common.data.JdbcRepositoryWrapper;

import java.util.List;

public class ScoreRule implements Rule<ScoreExchange,ScoreRuleOutput> {
    private static String SELECTSCORERULE="SELECT SCORETEXT FROM scorerule WHERE ?>=MINSCORE AND ?<=MAXSCORE LIMIT 1";
    private JdbcRepositoryWrapper jdbcRepositoryWrapper;
    public ScoreRule(JdbcRepositoryWrapper jdbcRepositoryWrapper)
    {
        this.jdbcRepositoryWrapper = jdbcRepositoryWrapper;
    }

    public void apply(ScoreExchange exchange, Handler<AsyncResult<ScoreRuleOutput>> handler)
    {
        System.out.println("applying oneliner rule");
        JsonArray params = new JsonArray();
        params.add(exchange.getScore());
        params.add(exchange.getScore());
        Future<List<JsonObject>> listFuture = jdbcRepositoryWrapper.retrieveMany(params, SELECTSCORERULE);
        ScoreRuleOutput scoreRuleOutput = new ScoreRuleOutput();
        scoreRuleOutput.setPercentile(exchange.getPercentile());
        listFuture.setHandler(x->{
            if (x.succeeded())
            {
                if( x.result().size()>0)
                {
                    System.out.println("size greater than 0");
                    System.out.println(x.result().get(0).toString());
                    String scoretext = x.result().get(0).getString("SCORETEXT");
                    scoreRuleOutput.setScoreAdjective(scoretext);
                    System.out.println(scoretext);
                }
                handler.handle(Future.succeededFuture(scoreRuleOutput));
            }
            else
            {
                System.out.println(x.cause());
                handler.handle(Future.failedFuture(x.cause()));
            }
        });
    }

}
