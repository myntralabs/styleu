package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.FaceApiFacade;
import com.myntra.style.styleanalysis.FaceApiRequest;
import com.myntra.style.styleanalysis.FaceApiResponse;
import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.*;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.impl.HttpClientImpl;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.impl.WebClientImpl;
import java.util.Base64;
import io.vertx.core.buffer.Buffer;
import org.apache.logging.log4j.util.Strings;


public class FaceApiFacadeImpl implements FaceApiFacade {
    private static final String subscriptionKey = "1a239bd64687448c92ac0b09e5400078";
    private static final String uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=age,gender";
    private static final String uriRelativePath ="face/v1.0/detect";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String OCP_APIM_SUBSCRIPTION_KEY= "Ocp-Apim-Subscription-Key";
    private Vertx vertx;
    private JsonObject config;

    public FaceApiFacadeImpl(Vertx vertx, JsonObject config)
    {
        this.vertx = vertx;
        this.config = config;
    }

    @Override
    public void analyze(FaceApiRequest apiRequest, Handler<AsyncResult<FaceApiResponse>> handler) {
           byte[] decode = Base64.getDecoder().decode(apiRequest.getImage());
           Buffer buffer = Buffer.buffer(decode);
           WebClient webClient = WebClient.create(vertx);
           webClient.postAbs(uriBase).
                putHeader(CONTENT_TYPE_HEADER,"application/octet-stream").putHeader(OCP_APIM_SUBSCRIPTION_KEY,subscriptionKey).
                sendBuffer(buffer,postresponse-> {
                    if (postresponse.succeeded())
                    {
                         System.out.println("post to face api success");
                         Buffer body = postresponse.result().body();
                         String string = body.getString(0, body.length());
                         System.out.println(string);
                         FaceApiResponse response = constructFaceApiResponse(string);
                         if (Strings.isBlank(response.getGender()))
                         {
                             JsonObject errorResponse = ResponseUtil.getErrorResponse("900", "COULD NOT DETECT FACE");
                             System.out.print("---- Before");
                             System.out.println(errorResponse.toString());
                             handler.handle(Future.failedFuture(errorResponse.toString()));
                         }
                         else {
                             System.out.println(response.toJson());
                             System.out.print("xxxx Before");
                             handler.handle(Future.succeededFuture(response));
                         }
                    }else
                    {
                        JsonObject errorResponse = ResponseUtil.getErrorResponse("901", "SYSTEM FAILURE - COULD NOT DETECT FACE");
                        System.out.println("post to face api failed");
                        handler.handle(Future.failedFuture(errorResponse.toString()));
                    }
        });
    }

    private FaceApiResponse constructFaceApiResponse(String responseStr) {
        FaceApiResponse response = null;
        try {
            JsonArray jsonArray = new JsonArray(responseStr);

            return new FaceApiResponse(jsonArray);
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            return new FaceApiResponse();
        }
    }
}
