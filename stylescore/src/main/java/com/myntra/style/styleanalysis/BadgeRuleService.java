package com.myntra.style.styleanalysis;

import com.myntra.style.styleanalysis.impl.BadgeRuleServiceImpl;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

@ProxyGen
@VertxGen
public interface BadgeRuleService {
    /**
     * The name of the event bus service.
     */
    String SERVICE_NAME = "badgeruleservice-eb-service";

    /**
     * The address on which the service is published.
     */
    String SERVICE_ADDRESS = "service.badgeruleservice";

    void evaluate(BadgeRuleEvaluationEntity ruleEvaluationEntity,Handler<AsyncResult<Badge>> resultHandler);

    static BadgeRuleService create(Vertx vertx, JsonObject config) {
        return new BadgeRuleServiceImpl(vertx,config);
    }

    static BadgeRuleService createProxy(Vertx vertx, String address) {
        return new BadgeRuleServiceVertxEBProxy(vertx,address); // TODO: to be rep;lace by proxy
    }
}

