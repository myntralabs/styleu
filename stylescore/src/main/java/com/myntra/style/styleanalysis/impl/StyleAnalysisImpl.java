package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.*;
import com.myntra.style.styleanalysis.rules.ImageAnalysisExchange;
import com.myntra.style.styleanalysis.rules.ImageAnalysisRuleImpl;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import myntra.vertx.common.data.JdbcRepositoryWrapper;
import org.apache.logging.log4j.util.Strings;

import java.util.LinkedList;

public class StyleAnalysisImpl implements StyleAnalysisService{

    private BadgeRuleService badgeRuleService;
    private ImageAnalysisService imageAnalysisService;
    private Vertx vertx;
    private JsonObject config;
    private FaceApiFacade faceApiFacade;
    private LooksApiFacade looksApiFacade;
    private JdbcRepositoryWrapper jdbcRepositoryWrapper;
    private ImageAnalysisRuleImpl rule;
    public StyleAnalysisImpl(Vertx vertx, BadgeRuleService badgeRuleService,FaceApiFacade faceApiFacade,JsonObject config)
    {
      this.vertx = vertx;
      this.badgeRuleService = badgeRuleService;
      this.imageAnalysisService = new ImageAnalysisServiceImpl(vertx,config);
      this.looksApiFacade = new LooksApiFacadeImpl(vertx,config);
      this.faceApiFacade = faceApiFacade;
      this.config = config;
      rule = new ImageAnalysisRuleImpl(vertx,config);
     // this.jdbcRepositoryWrapper = new JdbcRepositoryWrapper(vertx,config);
    }

    @Override
    public StyleAnalysisService analyse(StyleAnalysis styleAnalysis, Handler<AsyncResult<StyleAnalysis>> resultHandler) {
        Future future = Future.future();

        future.setHandler(resultHandler);
        Future faceApiCompleter = Future.future();
        communicateWithFaceApi(styleAnalysis,faceApiCompleter);
        faceApiCompleter.compose(u->{
            Future imageApiCompleter = Future.future();
            communicateWithImageApi((StyleAnalysis)u,imageApiCompleter.completer());
            return imageApiCompleter;
        }).compose(v->{
            Future looksApiCompleter = Future.future();
            communicateWithLooksApi((StyleAnalysis)v,looksApiCompleter.completer());
            return looksApiCompleter;
        }).compose(w->{
            communicateWithRules((StyleAnalysis)w,future.completer());
        },future);

        return this;
    }

    @Override
    public StyleAnalysisService store(StyleAnalysis styleAnalysis, Handler<AsyncResult<StyleAnalysis>> resultHandler) {

      StyleAnalysisPersistenceEntity persistenceEntity =  this.convertToStyleAnalysisPersistenceEntity(styleAnalysis);
      return this;
    }

    private StyleAnalysisPersistenceEntity convertToStyleAnalysisPersistenceEntity(StyleAnalysis styleAnalysis) {

      return new StyleAnalysisPersistenceEntity();
    }


    private void communicateWithFaceApi(StyleAnalysis styleAnalysis,Handler<AsyncResult<StyleAnalysis>> resultHandler )
    {
        FaceApiRequest request = new FaceApiRequest();
        request.setImage(styleAnalysis.getEncodeImage());

        faceApiFacade.analyze(request,faceapihandler->{
            if (faceapihandler.succeeded())
            {
                System.out.println("Face api call succeeded");
                FaceApiResponse faceApiResponse = faceapihandler.result();
                styleAnalysis.setGender(faceApiResponse.getGender());
                styleAnalysis.setFaceAttribute(faceApiResponse.toJson());
                styleAnalysis.setFaceRectangle(faceApiResponse.getFaceRectangle());
                resultHandler.handle(Future.succeededFuture(styleAnalysis));
            }
            else
            {
                //System.out.print(faceapihandler.result());
                resultHandler.handle(Future.failedFuture("face api call failed"));
            }
        });

    }


    private void communicateWithImageApi(StyleAnalysis styleAnalysis,Handler<AsyncResult<StyleAnalysis>> resultHandler )
    {
        System.out.println("calling image api");
        imageAnalysisService.analyze(styleAnalysis,imageanalysishandler->{
            if (imageanalysishandler.succeeded())
            {
                System.out.println("image analysis api call succeeded");
                JsonObject result = imageanalysishandler.result();
                styleAnalysis.setAnalysisJson(result);
               // System.out.println(result);
                resultHandler.handle(Future.succeededFuture(styleAnalysis));
            }
            else
            {
                System.out.println("image analysis api call failed");
                resultHandler.handle(Future.failedFuture(imageanalysishandler.cause()));
            }
        });
    }

    private void communicateWithRules(StyleAnalysis styleAnalysis,Handler<AsyncResult<StyleAnalysis>> resultHandler)
    {
        ImageAnalysisExchange exchange = this.getImageRuleExchange(styleAnalysis);
        this.rule.apply(exchange,x->{
            if (x.succeeded())
            {
                System.out.println("applied rules");
                System.out.print(x.result().getOneline().getOnelinercategory());
                styleAnalysis.setScoreAnalysis(new JsonObject(Json.encode(x.result())));
                System.out.println(x.result());
                resultHandler.handle(Future.succeededFuture(styleAnalysis));
            }
            else {
                System.out.println("application of rules failed");
                System.out.println(x.cause().fillInStackTrace());
                resultHandler.handle(Future.failedFuture(x.cause()));
            }

        });

    }

    private ImageAnalysisExchange getImageRuleExchange(StyleAnalysis styleAnalysis) {
        ImageAnalysisExchange exchange = new ImageAnalysisExchange();
        System.out.println("--- " + styleAnalysis.getAnalysisJson().getJsonObject("scores").getDouble("overall_score"));
        exchange.setScore(styleAnalysis.getAnalysisJson().getJsonObject("scores").getDouble("overall_score"));
        return  exchange;
    }

    private void communicateWithLooksApi(StyleAnalysis styleAnalysis, Handler<AsyncResult<StyleAnalysis>> resultHandler)
    {
        LooksApiRequest apiRequest = this.convertToApiRequest(styleAnalysis);
        this.looksApiFacade.analyze(apiRequest,looksApiHandler->{
            if (looksApiHandler.succeeded())
            {
                System.out.println("looks api call succeeded");
                LooksApiResponse result = looksApiHandler.result();
                styleAnalysis.setLooks(result.toJson());
                resultHandler.handle(Future.succeededFuture(styleAnalysis));

            } else
            {
             System.out.println("looks api");
             resultHandler.handle(Future.failedFuture(looksApiHandler.cause()));
            }
        });
    }

    public LooksApiRequest convertToApiRequest(StyleAnalysis styleAnalysis)
    {
        LooksApiRequest request = new LooksApiRequest();
        JsonArray analysis = styleAnalysis.getAnalysisJson().getJsonArray("analysis");
        for(int k=0;k<analysis.size();k++)
        {
            JsonObject jsonObject = analysis.getJsonObject(k);
            String category = jsonObject.getString("category");
            String type = jsonObject.getString("type");
            if (Strings.isBlank(type))
            {
                type = category;
            }

            if (jsonObject!=null)
            {
                JsonArray similarstyles = jsonObject.getJsonArray("similar_styles");
                for(int l=0; l < similarstyles.size(); l++)
                {
                    request.addToCategory(type, similarstyles.getString(l));
                }
            }
        }

        return request;
    }

}

