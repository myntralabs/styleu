package com.myntra.style.styleanalysis;

import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import myntra.vertx.common.RestAPIVerticle;

import java.util.Set;

public class StyleAnalysisApi extends RestAPIVerticle {
    public static final String SERVICE_NAME = "styleanalysis-rest-api";
    private static final String API_ANALYZE = "/analyze/:deviceid";
    private static final String HEALTH_CHECK ="/healthcheck";
    private StyleAnalysisService styleAnalysisService;
    private static final Logger logger = LoggerFactory.getLogger(StyleAnalysisApi.class);

    public StyleAnalysisApi(StyleAnalysisService styleAnalysisService)
    {
        this.styleAnalysisService = styleAnalysisService;
    }
    @Override
    public void start(Future<Void> future) throws Exception {
        super.start();
        final Router router = Router.router(vertx);
        // body handler
        router.route().handler(BodyHandler.create());
        // API route handler
        router.post(API_ANALYZE).handler(BodyHandler.create().setMergeFormAttributes(true));
        router.post(API_ANALYZE).handler(this::apiAnalyze);
        router.get(HEALTH_CHECK).handler(this::apiHealthCheck);


        // get HTTP host and port from configuration, or use default value
        String host = config().getString("styleanalysis.http.address", "0.0.0.0");
        int port = config().getInteger("styleanalysis.http.port", 18080);

        // create HTTP server and publish REST service
        createHttpServer(router, host, port)
                .compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port))
                .setHandler(future.completer());
    }

    private void apiHealthCheck(RoutingContext routingContext)
    {
        routingContext.response().setStatusCode(201).putHeader("content-type", "application/json")
            .end();
    }

    private void apiAnalyze(RoutingContext context) {
        try {
            System.out.println("In api analyze");
            Set<FileUpload> fileUploads = context.fileUploads();
            Buffer body = context.getBody();
            String deviceid = context.request().getParam("deviceid");
            String imageEncoded = context.request().getFormAttribute("image");
            //String imageEncoded = body.getString(0, body.length(), "UTF-8");
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("imageEncoded",imageEncoded);
            jsonObject.put("deviceid",deviceid);

            StyleAnalysis styleAnalysis = new StyleAnalysis(jsonObject);
            this.styleAnalysisService.analyse(styleAnalysis,r-> {
                if (r.succeeded()) {
                    logger.info("Preparing response to be sent");
                    System.out.print("preparing response to be sent");
                    // System.out.println(r.result().toJson().encodePrettily());
                    StyleAnalysis result = r.result();
                    result.setEncodeImage("");
                    System.out.println("+++ getting result");
                    System.out.println(Json.encode(result));
                    System.out.println("+++ sending result");
                    context.response().setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(Json.encode(result));
                }
                else {
                    logger.error(r.cause());
                    context.response().setStatusCode(500).end();
                }
            });

        } catch (Exception e) {
            context.response().setStatusCode(500).end();
        }
    }

}
