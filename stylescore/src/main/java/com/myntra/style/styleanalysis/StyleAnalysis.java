package com.myntra.style.styleanalysis;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;


@DataObject(generateConverter = true)
public class StyleAnalysis {
    private JsonObject analysisJson;
    private JsonObject scoreAnalysis;
    private JsonObject faceAttribute;
    private String deviceId;
    private String gender;
    private JsonObject looks;
    private String encodeImage;

    public JsonObject getFaceRectangle() {
        return faceRectangle;
    }

    public void setFaceRectangle(JsonObject faceRectangle) {
        this.faceRectangle = faceRectangle;
    }

    private JsonObject faceRectangle;

    public JsonObject getLooks() {
            return looks;
    }

    public void setLooks(JsonObject looks) {
            this.looks = looks;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEncodeImage() {
        return encodeImage;
    }

    public JsonObject getFaceAttribute() {
        return faceAttribute;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setFaceAttribute(JsonObject faceAttribute) {
        this.faceAttribute = faceAttribute;
    }

    public JsonObject getAnalysisJson() {
        return analysisJson;
    }

    public void setEncodeImage(String encodeImage) {
        this.encodeImage = encodeImage;
    }

    public void setAnalysisJson(JsonObject analysisJson) {
        this.analysisJson = analysisJson;
    }
    public JsonObject getScoreAnalysis() {
        return scoreAnalysis;
    }

    public void setScoreAnalysis(JsonObject scoreAnalysis) {
        this.scoreAnalysis = scoreAnalysis;
    }

    public StyleAnalysis()
    {

    }
    public StyleAnalysis(JsonObject jsonObject)
    {
        this.setEncodeImage(jsonObject.getString("imageEncoded"));
        this.setAnalysisJson(jsonObject.getJsonObject("analysis"));
        this.setScoreAnalysis(jsonObject.getJsonObject("badge"));
    }

    public JsonObject toJson() {
      JsonObject jsonObject = new JsonObject();
      //jsonObject.put("imageEncode",this.getEncodeImage());
       System.out.println("**** adding analysis") ;
      jsonObject.put("analysis",analysisJson);
        System.out.println("**** adding scoreAnalysis") ;
      jsonObject.put("badge", scoreAnalysis);
        System.out.println("**** adding face attribute") ;
      jsonObject.put("faceAttribute",jsonObject);
        System.out.println("**** adding looks") ;
      jsonObject.put("looks",looks);
      return jsonObject;
    }
}
