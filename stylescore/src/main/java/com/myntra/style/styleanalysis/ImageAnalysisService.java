package com.myntra.style.styleanalysis;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public interface ImageAnalysisService {
 void analyze(StyleAnalysis analysis, Handler<AsyncResult<JsonObject>> handler);
}
