package com.myntra.style.styleanalysis.rules;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import myntra.vertx.common.data.JdbcRepositoryWrapper;

import java.util.Random;

public class ImageAnalysisRuleImpl implements Rule<ImageAnalysisExchange,ImageRuleOutput> {
    private Rule<BadgeRuleExchange,BadgeRuleOutput> badgeRule;
    private Rule<BadgeNudgeExchange,BadgeNudgeOutput> badgeNudgeRule = new BadgeNudgeRule();
    private Rule<ScoreExchange,ScoreRuleOutput> scoreRule;
    private Rule<OnelinerExchange,OnelinerOutput> oneLinerRule;
    private Vertx vertx;
    private JsonObject config;
    private JdbcRepositoryWrapper wrapper;

    public ImageAnalysisRuleImpl(Vertx vertx, JsonObject config)
    {
        this.vertx = vertx;
        this.config = config;
        JsonObject object =  new JsonObject()
                .put("url", "jdbc:mysql://localhost:3306/styleu" )
                .put("driver_class", "com.mysql.jdbc.Driver")
                .put("max_pool_size", 3)
                .put("user","root")
                .put("password","password");
        this.wrapper = new JdbcRepositoryWrapper(vertx,object);
        oneLinerRule = new OnelinerRule(wrapper);
        this.scoreRule = new ScoreRule(wrapper);
        this.badgeRule = new BadgeRule(wrapper);
    }

    public Rule<BadgeRuleExchange, BadgeRuleOutput> getBadgeRule() {
        return badgeRule;
    }

    public void setBadgeRule(Rule<BadgeRuleExchange, BadgeRuleOutput> badgeRule) {
        this.badgeRule = badgeRule;
    }

    @Override
    public void apply(ImageAnalysisExchange exchange, Handler<AsyncResult<ImageRuleOutput>> handler) {
        System.out.println("applying image analysis rule");
        ImageRuleOutput imageRuleOutput = new ImageRuleOutput();
        this.spoofvist(exchange);
        this.spoofMileStoneScore(exchange);
        this.spoofPercentile(exchange);
         Future future = Future.future();
         future.setHandler(handler);
         Future badgeRuleFuture = Future.future();
         this.applyBadgeRule(exchange,imageRuleOutput,badgeRuleFuture);
        badgeRuleFuture.compose(y->{
            Future badgeNudgeFuture = Future.future();
            this.applyBadgeNudgeRule(exchange,imageRuleOutput,badgeNudgeFuture.completer());
            return badgeNudgeFuture;
        }).compose(z->{
            Future applyOneLiner = Future.future();
            this.applyOneLinerRule(exchange,imageRuleOutput,applyOneLiner.completer());
            return applyOneLiner;
        }).compose(x->
            {
            System.out.println(imageRuleOutput.getOneline().getOnelinercategory());
            this.applyScoreRule(exchange,imageRuleOutput,future.completer());
            },future);
    }

    private void spoofvist(ImageAnalysisExchange exchange) {
        Random random = new Random();
        exchange.setCurrentVisitNumber(random.nextInt(3));
    }

    private void spoofMileStoneScore(ImageAnalysisExchange exchange) {
        int  mileStoneScore =((int)exchange.getScore() / 5) * 5;
        double  mileStonePercentile = (((int)exchange.getPercentile()/5)) * 5;
        exchange.setNearestMileStoneScore(mileStoneScore);
        exchange.setNearestMileStonePercentile(mileStonePercentile);
    }

    private void spoofPercentile(ImageAnalysisExchange exchange)
    {
        if(exchange.getScore() < 100 && exchange.getScore() > 96)
        {
            exchange.setPercentile(99);
        }
        else if (exchange.getScore() < 96 && exchange.getScore()>90)
        {
            exchange.setPercentile(96);
        } else if (exchange.getScore()<90 && exchange.getScore()>80)
        {
            exchange.setPercentile(90);
        }
        else
        {
            exchange.setPercentile(80);
        }
    }

    private void applyBadgeRule(ImageAnalysisExchange exchange,ImageRuleOutput output,Handler<AsyncResult<ImageRuleOutput>> handler)
    {
         BadgeRuleExchange ruleExchange = getBadgeExchange(exchange);
         this.badgeRule.apply(ruleExchange,y->{
             if (y.succeeded())
             {
                 output.setBadge(y.result());
             }
             handler.handle(Future.succeededFuture(output));
         });
    }

    private void applyBadgeNudgeRule(ImageAnalysisExchange exchange,ImageRuleOutput output,Handler<AsyncResult<ImageRuleOutput>> handler)
    {
        BadgeNudgeExchange badgeNudgeExchange = this.getBadgeNudgeExchange(exchange);
        this.badgeNudgeRule.apply(badgeNudgeExchange,c->{
            if (c.succeeded())
            {
                BadgeNudgeOutput result = c.result();
                output.setBadgeNudge(result);
            }
            handler.handle(Future.succeededFuture(output));
        });
    }

    private void applyOneLinerRule(ImageAnalysisExchange exchange,ImageRuleOutput output,Handler<AsyncResult<ImageRuleOutput>> handler)
    {
     OnelinerExchange onelinerExchange = this.getOnelinerExchange(exchange);
     this.oneLinerRule.apply(onelinerExchange,x->{
         if (x.succeeded())
         {
             OnelinerOutput result = x.result();
             output.setOneline(result);
         }
         handler.handle(Future.succeededFuture(output));
     });
    }

    private void applyScoreRule(ImageAnalysisExchange exchange, ImageRuleOutput output,Handler<AsyncResult<ImageRuleOutput>> handler)
    {
       ScoreExchange scoreExchange = this.getScoreExchange(exchange);
       this.scoreRule.apply(scoreExchange,y-> {
         if (y.succeeded()) {
             ScoreRuleOutput result = y.result();
             output.setScore(result);
         }
           handler.handle(Future.succeededFuture(output));
       });
    }

    private BadgeNudgeExchange getBadgeNudgeExchange(ImageAnalysisExchange exchange) {
        BadgeNudgeExchange badgeNudgeExchange = new BadgeNudgeExchange();
        badgeNudgeExchange.setDeviceid(exchange.getDeviceId());
        badgeNudgeExchange.setPercentile(exchange.getPercentile());
        badgeNudgeExchange.setScore(exchange.getScore());
        return badgeNudgeExchange;
    }

    private BadgeRuleExchange getBadgeExchange(ImageAnalysisExchange exchange) {
        BadgeRuleExchange ruleExchange = new BadgeRuleExchange();
        ruleExchange.setScore(exchange.getScore());
        ruleExchange.setDeviceid(exchange.getDeviceId());
        ruleExchange.setPercentile(exchange.getPercentile());
        ruleExchange.setCurrentVisitNumber(exchange.getCurrentVisitNumber());
        ruleExchange.setNearestMileStonePercentile(exchange.getNearestMileStonePercentile());
        ruleExchange.setNearestMileStoneScore(exchange.getNearestMileStoneScore());
        return ruleExchange;
    }

    private ScoreExchange getScoreExchange(ImageAnalysisExchange exchange) {
        ScoreExchange scoreExchange = new ScoreExchange();
        scoreExchange.setDeviceid(exchange.getDeviceId());
        scoreExchange.setPercentile(exchange.getPercentile());
        scoreExchange.setScore(exchange.getScore());
        return scoreExchange;
    }

    private OnelinerExchange getOnelinerExchange(ImageAnalysisExchange exchange)
    {
        OnelinerExchange onelinerExchange = new OnelinerExchange();
        onelinerExchange.setPercentile(exchange.getPercentile());
        onelinerExchange.setScore(exchange.getScore());
        onelinerExchange.setDeviceid(exchange.getDeviceId());
        return onelinerExchange;
    }
}
