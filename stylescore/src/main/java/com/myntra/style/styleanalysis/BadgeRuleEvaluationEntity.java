package com.myntra.style.styleanalysis;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import java.util.List;

@DataObject(generateConverter = true)
public class BadgeRuleEvaluationEntity {
     private String registrationId;
     private List<RegistrationBadge> registrationBadges;
     public BadgeRuleEvaluationEntity(JsonObject jsonObject)
     {
     }
     public JsonObject toJson()
     {
          JsonObject jsonObject = new JsonObject();
          return jsonObject;
     }

}
