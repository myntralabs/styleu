package com.myntra.style.styleanalysis;

import com.myntra.style.styleanalysis.impl.BadgeRuleServiceImpl;
import com.myntra.style.styleanalysis.impl.FaceApiFacadeImpl;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

@VertxGen
@ProxyGen
public interface FaceApiFacade {
    /**
     * The name of the event bus service.
     */
    String SERVICE_NAME = "faceapifacade-eb-service";

    /**
     * The address on which the service is published.
     */
    String SERVICE_ADDRESS = "service.faceapifacade";

    public void analyze(FaceApiRequest apiRequest, Handler<AsyncResult<FaceApiResponse>> handler);

    static FaceApiFacade create(Vertx vertx, JsonObject config) {
        return new FaceApiFacadeImpl(vertx,config);
    }

    static FaceApiFacade createProxy(Vertx vertx, String address) {
        return new FaceApiFacadeVertxEBProxy(vertx,address);
    }
}
