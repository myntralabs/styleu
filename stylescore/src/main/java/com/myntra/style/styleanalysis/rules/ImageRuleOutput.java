package com.myntra.style.styleanalysis.rules;

public class ImageRuleOutput {
    public BadgeRuleOutput getBadge() {
        return badge;
    }

    public void setBadge(BadgeRuleOutput badge) {
        this.badge = badge;
    }

    public BadgeNudgeOutput getBadgeNudge() {
        return badgeNudge;
    }

    public void setBadgeNudge(BadgeNudgeOutput badgeNudge) {
        this.badgeNudge = badgeNudge;
    }

    public ScoreRuleOutput getScore() {
        return score;
    }

    public void setScore(ScoreRuleOutput score) {
        this.score = score;
    }

    public OnelinerOutput getOneline() {
        return oneline;
    }

    public void setOneline(OnelinerOutput oneline) {
        this.oneline = oneline;
    }

    private BadgeRuleOutput badge;
    private BadgeNudgeOutput badgeNudge;
    private ScoreRuleOutput score;
    private OnelinerOutput oneline;
}
