package com.myntra.style.styleanalysis;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
@DataObject(generateConverter = true)
public class Badge {
    private String category;
    private String badgeId;
    private String uri;
    private String uriH;
    private String badge;
    private String badgeName;
    private String badgeDescription;

    public String getBadgeText() {
        return badgeText;
    }

    public void setBadgeText(String badgeText) {
        this.badgeText = badgeText;
    }

    private String badgeText;

    public String getBadgeName() {
        return badgeName;
    }

    public void setBadgeName(String badgeName) {
        this.badgeName = badgeName;
    }

    public String getBadgeDescription() {
        return badgeDescription;
    }

    public void setBadgeDescription(String badgeDescription) {
        this.badgeDescription = badgeDescription;
    }


    public String getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(String badgeId) {
        this.badgeId = badgeId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUriH() {
        return uriH;
    }

    public void setUriH(String uriH) {
        this.uriH = uriH;
    }

    public Badge(JsonObject jsonObject)
    {
       this.setCategory(jsonObject.getString("category"));
       this.setBadge(jsonObject.getString("badge"));
       this.setBadgeId(jsonObject.getString("badgeId"));
       this.setUri(jsonObject.getString("uri"));
       this.setUriH(jsonObject.getString("uriH"));
       this.setBadgeName(jsonObject.getString("badgeName"));
       this.setBadgeText(jsonObject.getString("badgeText"));
       this.setBadgeDescription(jsonObject.getString("badgeDescription"));
    }

    public JsonObject toJson()
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("category",this.getCategory());
        jsonObject.put("badge",this.getBadge());
        jsonObject.put("badgeId",this.getBadgeId());
        jsonObject.put("uri",this.getUri());
        jsonObject.put("uriH",this.getUriH());
        jsonObject.put("badgeName",this.getBadgeName());
        jsonObject.put("badgeDescription",this.getBadgeDescription());
        jsonObject.put("badgeText", this.getBadgeText());
        return jsonObject;
    }
}
