package com.myntra.style.styleanalysis.rules;

import com.sun.net.httpserver.Authenticator;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import myntra.vertx.common.data.JdbcRepositoryWrapper;

import java.util.List;

public class BadgeRule implements Rule<BadgeRuleExchange,BadgeRuleOutput> {
    private JdbcRepositoryWrapper wrapper;
    private static  String  BEGINNERSLUCK="SELECT BADGECATEGORY FROM badgerule WHERE ? <= MAXVISITNUMBER AND ? >= VISITMINSCORE LIMIT 1";
    private static String  PERFORMANCEBASED1="SELECT BADGECATEGORY FROM badgerule WHERE ? > MINPERCENTILE LIMIT 1";
    private static String  PERFORMANCEBASED2="SELECT BADGECATEGORY FROM badgerule WHERE ? > MINPERCENTILE AND ? > MILESTONEVISITNUMBER LIMIT 1";
    private static String PERFORMANCEBASED3="SELECT BADGECATEGORY FROM badgerule WHERE ? VISITMINSCORE LIMIT 1";
    private static String SELECTBADGE = "SELECT BADGECATEGORY,BADGENAME,BADGETEXT,BADGEONELINER FROM badge WHERE BADGECATEGORY=? LIMIT 1";
    public BadgeRule(JdbcRepositoryWrapper wrapper)
    {
        this.wrapper = wrapper;
    }

    public void apply(BadgeRuleExchange badgeRuleExchange, Handler<AsyncResult<BadgeRuleOutput>> handler)
    {
        System.out.println("applying badge rule");
        BadgeRuleOutput output = new BadgeRuleOutput();
        Future future = Future.future();
        future.setHandler(handler);
        Future beginnerLuck = Future.future();
        this.applyBeginnersLuck(badgeRuleExchange,output,beginnerLuck);
        beginnerLuck.compose(u->{
            System.out.println("Calling performance");
            System.out.println(Json.encode(output));
            Future performancebasedFuture = Future.future();
            this.applyPerformanceBased(badgeRuleExchange,output,performancebasedFuture.completer());
            return performancebasedFuture;
        }).compose(v->{
            Future applyTenureBasedFuture = Future.future();
            System.out.println("Calling tenure");
            System.out.println(Json.encode(output));
            this.applyTenureBased(badgeRuleExchange,output,applyTenureBasedFuture.completer());
            return applyTenureBasedFuture;
        }).compose(y->{
            System.out.println("Calling badge");
            System.out.println(Json.encode(output));
           this.getBadge(output,future.completer());
        },future);


    }


    private void applyBeginnersLuck(BadgeRuleExchange badgeRuleExchange,BadgeRuleOutput ruleOutput,Handler<AsyncResult<BadgeRuleOutput>> handler)
    {
        System.out.println("Applying beginners luck");
        JsonArray params = new JsonArray();
        params.add(badgeRuleExchange.getCurrentVisitNumber());
        params.add(badgeRuleExchange.getScore());
        System.out.println(badgeRuleExchange.getCurrentVisitNumber());
        System.out.println(badgeRuleExchange.getScore());
        Future<List<JsonObject>> listFuture = this.wrapper.retrieveMany(params, BEGINNERSLUCK);
        listFuture.setHandler(x->{
                            List<JsonObject> result = x.result();
                            System.out.println("Beginners luck size");
                            //System.out.println(result.size());
                            if (x.succeeded() && x.result().size()>0)
                            {
                                System.out.println("getting badge succeeded");
                                ruleOutput.setFoundBadge(true);
                                System.out.println(result.get(0).encodePrettily());
                                String badgecategory = result.get(0).getString("BADGECATEGORY");
                                System.out.println("++ badge category");
                                System.out.println(badgecategory);
                                ruleOutput.setBadgeCategory(badgecategory);
                                handler.handle(Future.succeededFuture(ruleOutput));
                            }
                            else
                            {
                                handler.handle(Future.succeededFuture(ruleOutput));
                            }
                     });

    }

    private void applyPerformanceBased(BadgeRuleExchange badgeRuleExchange,BadgeRuleOutput ruleOutput,Handler<AsyncResult<BadgeRuleOutput>> handler)
    {
       // if (ruleOutput.isFoundBadge())
       // {
            handler.handle(Future.succeededFuture(ruleOutput));
        //}

        System.out.println("Applying performance based rule");
        JsonArray params = new JsonArray();
       // handler.handle(Future.succeededFuture(ruleOutput));
    }

    private void applyTenureBased(BadgeRuleExchange badgeRuleExchange,BadgeRuleOutput ruleOutput,Handler<AsyncResult<BadgeRuleOutput>> handler)
    {
      //  if (ruleOutput.isFoundBadge())
        //{
            handler.handle(Future.succeededFuture(ruleOutput));
       // }
        System.out.println("Applying tenure based rule");
        JsonArray params = new JsonArray();
      //  handler.handle(Future.succeededFuture(ruleOutput));
    }


    private void getBadge(BadgeRuleOutput ruleOutput,Handler<AsyncResult<BadgeRuleOutput>> handler)
    {

        if (ruleOutput.isFoundBadge())
        {
            System.out.print("++Getting badge");
            JsonArray params = new JsonArray();
            params.add(ruleOutput.getBadgeCategory());
            System.out.print(ruleOutput.getBadgeCategory());
            Future<List<JsonObject>> listFuture = this.wrapper.retrieveMany(params, SELECTBADGE);
            listFuture.setHandler(x->{
                List<JsonObject> result = x.result();
                if (x.succeeded() && result.size()>0)
                {
                    System.out.println("badge found");
                    System.out.print(Json.encode(x.result()));
                    String badgename = result.get(0).getString("BADGENAME");
                    String badgetext = result.get(0).getString("BADGETEXT");
                    String badgeoneliner = result.get(0).getString("BADGEONELINER");
                    ruleOutput.setBadgeName(badgename);
                    ruleOutput.setBadgeText(badgetext);
                    ruleOutput.setBadgeOneLine(badgeoneliner);
                    ruleOutput.setBadgeUrlH("");
                    ruleOutput.setBadgeUrlL("");
                    ruleOutput.setEarnedBadge(2);
                    System.out.println("Getting badge");
                    System.out.println(Json.encode(ruleOutput));
                } else{
                    ruleOutput.setFoundBadge(false);
                }
            });
        }

        handler.handle(Future.succeededFuture(ruleOutput));
    }


}
