package com.myntra.style.styleanalysis;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject(generateConverter = true)
public class FaceApiRequest {

    public FaceApiRequest()
    {

    }
    public FaceApiRequest(JsonObject jsonObject)
    {

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public JsonObject toJson()
    {
        return new JsonObject();
    }

    private String image;
}
