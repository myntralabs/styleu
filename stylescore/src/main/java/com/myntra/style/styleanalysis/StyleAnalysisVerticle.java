package com.myntra.style.styleanalysis;

import com.myntra.style.styleanalysis.impl.BadgeRuleServiceImpl;
import com.myntra.style.styleanalysis.impl.FaceApiFacadeImpl;
import com.myntra.style.styleanalysis.impl.StyleAnalysisImpl;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.serviceproxy.ProxyHelper;
import myntra.vertx.common.BaseMicroserviceVerticle;

public class StyleAnalysisVerticle extends BaseMicroserviceVerticle {

    @Override
    public void start(Future<Void> future) throws Exception {
        super.start();
        BadgeRuleService badgeRuleService = new BadgeRuleServiceImpl(vertx,config());

        // Badge Rule service
        ProxyHelper.registerService(BadgeRuleService.class, vertx,badgeRuleService,BadgeRuleService.SERVICE_ADDRESS);

        FaceApiFacade faceApiFacade = new FaceApiFacadeImpl(vertx,config());

        // register face api facade
        ProxyHelper.registerService(FaceApiFacade.class,vertx,faceApiFacade,FaceApiFacade.SERVICE_ADDRESS);

        // create the service instance
        StyleAnalysisService styleAnalysisService = new StyleAnalysisImpl(vertx,badgeRuleService,faceApiFacade,config());

        // register the service proxy on event bus
        ProxyHelper.registerService(StyleAnalysisService.class, vertx, styleAnalysisService,StyleAnalysisService.SERVICE_ADDRESS);

        // publish the service in the discovery infrastructure
        publishEventBusService(BadgeRuleService.SERVICE_NAME,BadgeRuleService.SERVICE_ADDRESS,BadgeRuleService.class);

        publishEventBusService(FaceApiFacade.SERVICE_NAME,FaceApiFacade.SERVICE_ADDRESS,FaceApiFacade.class);

         publishEventBusService(StyleAnalysisService.SERVICE_NAME, StyleAnalysisService.SERVICE_ADDRESS, StyleAnalysisService.class)
        .compose(servicePublished -> deployRestService(styleAnalysisService))
        .setHandler(future.completer());
    }

    private Future<Void> deployRestService(StyleAnalysisService service) {
        Future<String> future = Future.future();
        vertx.deployVerticle(new StyleAnalysisApi(service),
                new DeploymentOptions().setConfig(config()),
                future.completer());
        return future.map(r -> null);
    }
}
