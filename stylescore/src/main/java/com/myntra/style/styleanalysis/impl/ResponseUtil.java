package com.myntra.style.styleanalysis.impl;

import com.sun.net.httpserver.Authenticator;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

public class ResponseUtil {

    public static JsonObject getErrorResponse(String errorCode, String errorMessage)
    {
        JsonObject object = new JsonObject();
        object.put("status","error");
        object.put("errorCode",errorCode);
        object.put("errorMessage",errorMessage);
        JsonObject errorJson = new JsonObject();
        errorJson.put("responseHeader",object);
        return errorJson;
    }

    public static JsonObject getSuccessResponse(JsonObject jsonObject)
    {
        JsonObject object = new JsonObject();
        object.put("status","ok");
        jsonObject.put("responseHeader",object);
        return jsonObject;
    }
}
