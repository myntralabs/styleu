package com.myntra.style.styleanalysis;

import com.myntra.style.styleanalysis.impl.StyleAnalysisImpl;
import io.vertx.codegen.annotations.Fluent;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

@VertxGen
@ProxyGen
public interface StyleAnalysisService {
    /**
     * The name of the event bus service.
     */
    String SERVICE_NAME = "styleanalysis-eb-service";

    /**
     * The address on which the service is published.
     */
    String SERVICE_ADDRESS = "service.styleanalysis";

    @Fluent
    StyleAnalysisService analyse(StyleAnalysis styleAnalysis,Handler<AsyncResult<StyleAnalysis>> resultHandler);

    @Fluent
    StyleAnalysisService store(StyleAnalysis styleAnalysis,Handler<AsyncResult<StyleAnalysis>> resultHandler);

    static StyleAnalysisService create(Vertx vertx, BadgeRuleService badgeRuleService,FaceApiFacade faceApiFacade, JsonObject config) {
        return new StyleAnalysisImpl(vertx,badgeRuleService,faceApiFacade,config);
    }

    static StyleAnalysisService createProxy(Vertx vertx, String address) {
        return new StyleAnalysisServiceVertxEBProxy(vertx, address);
    }
}
