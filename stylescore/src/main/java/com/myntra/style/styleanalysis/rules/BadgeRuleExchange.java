package com.myntra.style.styleanalysis.rules;

public class BadgeRuleExchange {

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    private String deviceid;
    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getPercentile() {
        return percentile;
    }

    public void setPercentile(double percentile) {
        this.percentile = percentile;
    }

    private double score;
    private double percentile;
    private int currentVisitNumber;
    private int nearestMileStoneScore;
    private double nearestMileStonePercentile;

    public int getCurrentVisitNumber() {
        return currentVisitNumber;
    }

    public void setCurrentVisitNumber(int currentVisitNumber) {
        this.currentVisitNumber = currentVisitNumber;
    }

    public int getNearestMileStoneScore() {
        return nearestMileStoneScore;
    }

    public void setNearestMileStoneScore(int nearestMileStoneScore) {
        this.nearestMileStoneScore = nearestMileStoneScore;
    }

    public double getNearestMileStonePercentile() {
        return nearestMileStonePercentile;
    }

    public void setNearestMileStonePercentile(double nearestMileStonePercentile) {
        this.nearestMileStonePercentile = nearestMileStonePercentile;
    }


}
