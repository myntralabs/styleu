package com.myntra.style.styleanalysis.rules;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import myntra.vertx.common.data.JdbcRepositoryWrapper;

import java.util.List;

public class OnelinerRule implements Rule<OnelinerExchange,OnelinerOutput>{

    private static String SELECTONELINERCATEGORY ="SELECT ONELINERCATEGORY FROM onelinerrule WHERE ?>=MINSCORE AND ?<=MAXSCORE LIMIT 1";
    private static String SELECTONELINER ="SELECT ONELINERTEXT FROM onelinerconfig WHERE CATEGORY=? LIMIT 1";
    private JdbcRepositoryWrapper jdbcRepositoryWrapper;
    public OnelinerRule(JdbcRepositoryWrapper jdbcRepositoryWrapper)
    {
        this.jdbcRepositoryWrapper = jdbcRepositoryWrapper;
    }

    public void apply(OnelinerExchange exchange, Handler<AsyncResult<OnelinerOutput>> handler)
    {
        System.out.println("applying oneliner rule");
        OnelinerOutput onelinerOutput = new OnelinerOutput();
        Future future = Future.future();
        future.setHandler(handler);
        Future getOnelinerCategoryFuture = Future.future();
        this.getCategory(exchange,getOnelinerCategoryFuture);
        getOnelinerCategoryFuture.compose(x->{
         this.getOneliner((OnelinerOutput)x,future.completer());
        },future);
    }

    private void getCategory(OnelinerExchange exchange, Handler<AsyncResult<OnelinerOutput>> handler)
    {
        JsonArray params = new JsonArray();
        params.add(exchange.getScore());
        params.add(exchange.getScore());
        Future<List<JsonObject>> listFuture = jdbcRepositoryWrapper.retrieveMany(params, SELECTONELINERCATEGORY);
        OnelinerOutput onelinerOutput = new OnelinerOutput();
        listFuture.setHandler(x->{
            if (x.succeeded())
            {
                if(x.result()!=null && x.result().size()>0)
                {
                    System.out.println("size greater than 0");
                    System.out.println(x.result().get(0).toString());
                    String onelinercategory = x.result().get(0).getString("ONELINERCATEGORY");
                    onelinerOutput.setOnelinercategory(onelinercategory);
                }
                handler.handle(Future.succeededFuture(onelinerOutput));
            }
            else
            {
                System.out.println(x.cause());
                handler.handle(Future.failedFuture(x.cause()));
            }
        });

    }

    private void getOneliner(OnelinerOutput onelinerOutput, Handler<AsyncResult<OnelinerOutput>> handler)
    {
        JsonArray params = new JsonArray();
        params.add(onelinerOutput.getOnelinercategory());
        Future<List<JsonObject>> listFuture = jdbcRepositoryWrapper.retrieveMany(params, SELECTONELINER);
        listFuture.setHandler(x->{
            if (x.succeeded())
            {
                if( x.result().size()>0)
                {
                    System.out.println("size greater than 0");
                    System.out.println(x.result().get(0).toString());
                    String onelinertext = x.result().get(0).getString("ONELINERTEXT");
                    onelinerOutput.setOneLinerText(onelinertext);
                }
                handler.handle(Future.succeededFuture(onelinerOutput));
            }
            else
            {
                System.out.println(x.cause());
                handler.handle(Future.failedFuture(x.cause()));
            }
        });

    }
}
