package com.myntra.style.styleanalysis.rules;

public class BadgeNudgeOutput {
    public String getBadgeNudgeText() {
        return badgeNudgeText;
    }

    public void setBadgeNudgeText(String badgeNudgeText) {
        this.badgeNudgeText = badgeNudgeText;
    }

    private String badgeNudgeText;
}
