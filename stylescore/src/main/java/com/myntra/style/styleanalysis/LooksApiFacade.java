package com.myntra.style.styleanalysis;

import com.myntra.style.styleanalysis.impl.FaceApiFacadeImpl;
import com.myntra.style.styleanalysis.impl.LooksApiFacadeImpl;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
@VertxGen
@ProxyGen
public interface LooksApiFacade {
    /**
     * The name of the event bus service.
     */
    String SERVICE_NAME = "looksapifacade-eb-service";

    /**
     * The address on which the service is published.
     */
    String SERVICE_ADDRESS = "service.looksapi";

    void analyze(LooksApiRequest request, Handler<AsyncResult<LooksApiResponse>> resultHandler);
    static LooksApiFacade create(Vertx vertx, JsonObject config) {
        return new LooksApiFacadeImpl(vertx,config);
    }

    static LooksApiFacade createProxy(Vertx vertx, String address) {
        return new LooksApiFacadeVertxEBProxy(vertx,address);
    }

}
