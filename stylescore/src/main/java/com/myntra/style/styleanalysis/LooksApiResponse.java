package com.myntra.style.styleanalysis;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@DataObject(generateConverter = true)
public class LooksApiResponse {

    public JsonArray getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(JsonArray recommendations) {
        this.recommendations = recommendations;
    }

    private JsonArray recommendations;

//    public LooksApiResponse(JsonObject topwear,JsonObject bottomwear)
//    {
//        this.topwear = topwear;
//        this.bottomwear = bottomwear;
//    }
    public LooksApiResponse(JsonObject jsonObject)
    {

    }
    public LooksApiResponse()
    {

    }
    public JsonObject toJson()
    {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("recommendation",recommendations);

        return jsonObject;
    }

}
