package com.myntra.style.styleanalysis.rules;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

public interface  Rule<T,R> {
     void apply(T exchange, Handler<AsyncResult<R>> handler);
}
