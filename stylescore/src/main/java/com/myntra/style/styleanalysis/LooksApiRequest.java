package com.myntra.style.styleanalysis;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

@DataObject(generateConverter = true)
public class LooksApiRequest {

    private HashMap<String,java.util.ArrayList<String>> map = new LinkedHashMap<String,java.util.ArrayList<String>>();

    public HashMap<String,java.util.ArrayList<String>> getCategoryRecommendation()
    {
        return map;
    }

    public java.util.List<String> getTopwear() {
        return topwear;
    }

    public void setTopwear(java.util.List<String> topwear) {
        this.topwear = topwear;
    }

    private java.util.List<String> topwear;

    public java.util.List<String> getBottomwear() {
        return bottomwear;
    }

    public void setBottomwear(java.util.List<String> bottomwear) {
        this.bottomwear = bottomwear;
    }

    private java.util.List<String> bottomwear;

    public LooksApiRequest()
    {

    }

    public LooksApiRequest(java.util.List<String> topwear, java.util.List<String> bottomwear)
    {
        this.topwear = topwear;
        this.bottomwear = bottomwear;
    }

    public LooksApiRequest(JsonObject jsonObject)
    {
//        jsonObject.get
//        String topwear_reco = jsonObject.getString("topwear_reco");
//        String bottomwear_reco = jsonObject.getString("bottomwear_reco");
//        java.util.List<String> strings = Arrays.asList(topwear_reco.split(","));
//        this.setTopwear(strings);
    }

    public JsonObject toJson()
    {
        return  new JsonObject();
    }

    public void addCategoryRecommendation(String category, java.util.ArrayList<String> styleRecommendation)
    {
        this.map.put(category,styleRecommendation);
    }


    public void addToCategory(String category, String styleId)
    {
       if (this.map.get(category)==null)
       {
           this.map.put(category,new java.util.ArrayList<String>());
       }
       else
       {
          this.map.get(category).add(styleId);
       }
    }

}
