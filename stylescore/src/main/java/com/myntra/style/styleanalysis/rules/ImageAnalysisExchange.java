package com.myntra.style.styleanalysis.rules;


public class ImageAnalysisExchange {
    private String deviceId;
    private double score;
    private double percentile;
    private int currentVisitNumber;
    private int nearestMileStoneScore;
    private double nearestMileStonePercentile;

    public int getNearestMileStoneScore() {
        return nearestMileStoneScore;
    }

    public void setNearestMileStoneScore(int nearestMileStoneScore) {
        this.nearestMileStoneScore = nearestMileStoneScore;
    }

    public double getNearestMileStonePercentile() {
        return nearestMileStonePercentile;
    }

    public void setNearestMileStonePercentile(double nearestMileStonePercentile) {
        this.nearestMileStonePercentile = nearestMileStonePercentile;
    }

    public String getDeviceId() {
        return deviceId;
    }


    public int getCurrentVisitNumber() {
        return currentVisitNumber;
    }

    public void setCurrentVisitNumber(int currentVisitNumber) {
        this.currentVisitNumber = currentVisitNumber;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getPercentile() {
        return percentile;
    }

    public void setPercentile(double percentile) {
        this.percentile = percentile;
    }

    public ImageAnalysisExchange()
    {

    }
}
