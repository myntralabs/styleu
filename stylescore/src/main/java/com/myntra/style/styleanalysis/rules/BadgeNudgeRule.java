package com.myntra.style.styleanalysis.rules;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;

public class BadgeNudgeRule implements Rule<BadgeNudgeExchange,BadgeNudgeOutput> {

    public void apply(BadgeNudgeExchange badgeNudgeRule, Handler<AsyncResult<BadgeNudgeOutput>> handler)
    {
        System.out.println("applying badge nudge rule");
        handler.handle(Future.succeededFuture(new BadgeNudgeOutput()));
    }
}
