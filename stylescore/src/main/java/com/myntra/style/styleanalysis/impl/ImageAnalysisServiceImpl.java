package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.ImageAnalysisService;
import com.myntra.style.styleanalysis.StyleAnalysis;
import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class ImageAnalysisServiceImpl implements ImageAnalysisService {
    private JsonObject config;
    private Vertx vertx;
    private static String url = "http://52.74.70.144:8082/get_score";//"http://dsstyleu.myntra.com/get_score";
    private static int TIMEOUT = 60;

    public ImageAnalysisServiceImpl(Vertx vertx, JsonObject config)
    {
        this.vertx = vertx;
        this.config = config;
    }

    @Override
    public void analyze(StyleAnalysis styleAnalysis, Handler<AsyncResult<JsonObject>> handler) {
        MultiMap multiMap = MultiMap.caseInsensitiveMultiMap();
        multiMap.add("gender",styleAnalysis.getGender());
        multiMap.add("imgData",styleAnalysis.getEncodeImage());

        JsonObject payload  = new JsonObject();
        payload.put("gender",styleAnalysis.getGender());
        payload.put("imgData",styleAnalysis.getEncodeImage());
        payload.put("faceRectangle",styleAnalysis.getFaceRectangle());
        WebClient.create(vertx).postAbs(url).timeout(TIMEOUT*1000).sendJsonObject(payload,webHandler->{
                  if (webHandler.succeeded())
                  {
                      Buffer body = webHandler.result().body();
                      String analysis = body.getString(0, body.length());
                      JsonObject jsonObject = new JsonObject(analysis);
                      if (!validateResponse(jsonObject))
                      {
                       System.out.print("Obtained invalid response from visense analysis api");
                       handler.handle(Future.failedFuture("Style score not obtained"));
                      }
                      else {
                          System.out.println("Visense Response validated ");
                          handler.handle(Future.succeededFuture(jsonObject));
                      }
                  }
                  else
                  {
                      System.out.print("DS Image analysis api failed");
                      System.out.print(webHandler.cause());
                      handler.handle(Future.failedFuture(webHandler.cause()));
                  }
                });
    }

    private boolean validateResponse(JsonObject jsonObject) {
        JsonArray analysis = jsonObject.getJsonArray("analysis");
        JsonObject score = jsonObject.getJsonObject("scores");

        if (analysis==null || score==null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}
