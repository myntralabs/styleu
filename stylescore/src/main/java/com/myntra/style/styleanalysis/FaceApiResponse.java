package com.myntra.style.styleanalysis;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@DataObject()
public class FaceApiResponse {
    public FaceApiResponse()
    {

    }

    public FaceApiResponse (JsonArray jsonArray)
    {
        this.assignToProperties(jsonArray);
    }

    private void assignToProperties(JsonArray jsonArray) {
        JsonObject jsonObject = jsonArray.getJsonObject(0);
        this.constructFromJson(jsonObject);
    }

    public FaceApiResponse(JsonObject jsonObject)
    {
        constructFromJson(jsonObject);
    }

    private void constructFromJson(JsonObject jsonObject) {
        this.setFaceId(jsonObject.getString("faceId"));
        this.setGender(jsonObject.getJsonObject("faceAttributes").getString("gender"));
        this.setFaceRectangle(jsonObject.getJsonObject("faceRectangle"));
        this.setFaceApiResponse(jsonObject);
    }

    public JsonObject getFaceRectangle() {
        return faceRectangle;
    }

    public void setFaceRectangle(JsonObject faceRectangle) {
        this.faceRectangle = faceRectangle;
    }

    private JsonObject faceRectangle;
    public String getFaceId() {
        return faceId;
    }

    public void setFaceId(String faceId) {
        this.faceId = faceId;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public JsonObject toJson()
    {
       JsonObject x = new JsonObject();
       x.put("faceId",this.getFaceId());
       x.put("gender",this.getGender());
       x.put("faceRectangle",this.getFaceRectangle());
       return x;
    }

    public JsonObject getFaceApiResponse() {
        return faceApiResponse;
    }

    public void setFaceApiResponse(JsonObject faceApiResponse) {
        this.faceApiResponse = faceApiResponse;
    }

    private JsonObject faceApiResponse;
    private String faceId;
    private String gender;

}
