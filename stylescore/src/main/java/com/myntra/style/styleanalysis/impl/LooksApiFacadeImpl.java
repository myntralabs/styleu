package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.LooksApiFacade;
import com.myntra.style.styleanalysis.LooksApiRequest;
import com.myntra.style.styleanalysis.LooksApiResponse;
import com.myntra.style.styleanalysis.StyleAnalysis;
import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.core.buffer.Buffer;
import org.apache.logging.log4j.util.Strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class LooksApiFacadeImpl implements LooksApiFacade {

    private static int  LIMIT = 5;
    private static String looksApiUrl = "http://developer.myntra.com/looks/looks/select/";
    private Vertx vertx;
    private JsonObject config;
    private int LOOKSTIMEOUT=3;
    public LooksApiFacadeImpl(Vertx vertx, JsonObject config)
    {
        this.vertx = vertx;
        this.config = config;
    }

    @Override
    public void analyze(LooksApiRequest request, Handler<AsyncResult<LooksApiResponse>> resultHandler) {
        System.out.println("**looks api call");
        System.out.println(request.toJson());
        this.getResult(request,y->{
            if (y.succeeded())
            {
                resultHandler.handle(Future.succeededFuture(y.result()));
                System.out.println("calling after hanldler");
            }
            else
            {
                resultHandler.handle(Future.failedFuture(y.cause()));
                System.out.println("Calling after failed");
            }
        });

//        WebClient webClient = WebClient.create(vertx);
//        List<String> topwear = request.getTopwear();
//        List<String> bottomwear = request.getBottomwear();
//        String topwearRequest = this.convertToApiRequest(topwear);
//        String bottomRequest = this.convertToApiRequest(bottomwear);
//        JsonObject topwearrequest = new JsonObject(topwearRequest);
//        JsonObject bottomwearrequest = new JsonObject(bottomRequest);
//
//        HttpRequest<Buffer> bufferHttpRequest = webClient.postAbs(looksApiUrl).putHeader("Content-Type", "application/json");
//        bufferHttpRequest.sendBuffer(Buffer.buffer(topwearrequest.toString()), x -> {
//            LooksApiResponse apiResponse = new LooksApiResponse(new JsonObject());
//            if (x.succeeded()) {
//                System.out.println("Succeeded topwear");
//                System.out.println(topwearrequest.toString());
//                Buffer body = x.result().body();
//                String result = body.getString(0, body.length());
//                System.out.println(result);
//                apiResponse.setTopwear(new JsonObject(result).getJsonArray("data").getJsonObject(0));
//                bufferHttpRequest.sendBuffer(Buffer.buffer(bottomwearrequest.toString()),y->{
//                    if (y.succeeded()) {
//                        System.out.println(bottomwearrequest.toString());
//                        System.out.println("Succeeded bottomwear");
//                        Buffer body1 = y.result().body();
//                        String result1 = body1.getString(0,body1.length());
//                        apiResponse.setBottomwear((new JsonObject(result1)).getJsonArray("data").getJsonObject(0));
//                        resultHandler.handle(Future.succeededFuture(apiResponse));
//                    }
//                    else {
//                        System.out.println("failed");
//                        resultHandler.handle(Future.failedFuture(y.cause()));
//                   }
//                });
//            } else {
//                System.out.println("failed");
//                resultHandler.handle(Future.failedFuture(x.cause()));
//            }
//        });
    }

    private void getResult(LooksApiRequest apiRequest,Handler<AsyncResult<LooksApiResponse>> handler)
    {
        HashMap<String, ArrayList<String>> categoryRecommendation = apiRequest.getCategoryRecommendation();
        JsonArray array = new JsonArray(); // Synchronized json array
        LooksApiResponse response = new LooksApiResponse();
        AtomicInteger count = new AtomicInteger(0);
        System.out.print(Json.encode(apiRequest));
        for(String key :categoryRecommendation.keySet())
        {
            System.out.println("processing looks for category -> "+key);
            ArrayList<String> strings = categoryRecommendation.get(key);
            String request = this.convertToApiRequest(strings);
            System.out.print(request);
            WebClient webClient = WebClient.create(vertx);
            HttpRequest<Buffer> bufferHttpRequest = webClient.postAbs(looksApiUrl).timeout(LOOKSTIMEOUT*1000).putHeader("Content-Type", "application/json");
            bufferHttpRequest.sendBuffer(Buffer.buffer(request.toString()), y->{
                JsonObject jsonObject = new JsonObject();
                int runningCount = count.incrementAndGet();
                jsonObject.put("category",key);
                if (y.succeeded())
                {
                    System.out.println("**getting looks for category is successful : " + key);
                    Buffer body = y.result().body();
                    String result = body.getString(0,body.length());
                    boolean validateLooksRespone = validateLooksResponse(result);
                    if (validateLooksRespone) {
                        JsonObject data = new JsonObject(result).getJsonArray("data").getJsonObject(0);
                        jsonObject.put("looksAvailable", true);
                        jsonObject.put("recommendation", data);
                    } else {
                        jsonObject.put("looksAvailable", false);
                    }
                }
                else {
                    System.out.print("getting looks for category is failed : " + key);
                    jsonObject.put("looksAvailable", false);
                    System.out.println("--looks failed");
                    System.out.println(y.cause());
                }

                array.add(jsonObject);
                if(runningCount>=categoryRecommendation.keySet().size()) {
                    response.setRecommendations(array);
                    handler.handle(Future.succeededFuture(response));
                }
            });
        }

    }

    private boolean validateLooksResponse(String result) {

        try{
            JsonObject data = new JsonObject(result).getJsonArray("data").getJsonObject(0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    public String convertToApiRequest(List<String> styleIds)
    {
        JsonObject request = new JsonObject();
        request.put("offset",0L);
        request.put("limit",LIMIT);
        request.put("hasFlatshots",false);
        request.put("format", "tbaf");
        String join = String.join(",", styleIds);
        JsonArray array = new JsonArray(styleIds);
        request.put("selectFrom",array);//"["+join +"]");
        request.put("selectBy","styleids");
        return request.toString();
    }



    public LooksApiResponse convertToLooksApiResponse(JsonObject jsonObject)
    {
      return new LooksApiResponse();
    }
}
