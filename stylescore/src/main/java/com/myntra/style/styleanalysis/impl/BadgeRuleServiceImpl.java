package com.myntra.style.styleanalysis.impl;

import com.myntra.style.styleanalysis.Badge;
import com.myntra.style.styleanalysis.BadgeRuleEvaluationEntity;
import com.myntra.style.styleanalysis.BadgeRuleService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class BadgeRuleServiceImpl implements BadgeRuleService {

    private Vertx vertx;
    private JsonObject config;

    public BadgeRuleServiceImpl(Vertx vertx,JsonObject config)
    {
     this.vertx = vertx;
     this.config = config;
    }


    @Override
    public void evaluate(BadgeRuleEvaluationEntity ruleEvaluationEntity, Handler<AsyncResult<Badge>> resultHandler) {
         JsonObject jsonObject = new JsonObject();
         jsonObject.put("category","badge category");
         jsonObject.put("badge","badge_badge");
         jsonObject.put("badgeId", "badgeId");
         jsonObject.put("uri","uri");
         jsonObject.put("uriH","uriH");
         jsonObject.put("badgeName","badge name");
         jsonObject.put("badgeDescription","badge description");
         jsonObject.put("badgeText", "badge Text");
         Badge badge = new Badge(jsonObject);
         resultHandler.handle(Future.succeededFuture(badge));
    }
}
