package com.myntra.style.styleanalysis.rules;

public class BadgeRuleOutput {
    public String getBadgeOneLine() {
        return badgeOneLine;
    }

    public void setBadgeOneLine(String badgeOneLine) {
        this.badgeOneLine = badgeOneLine;
    }

    private String badgeOneLine;
    public String getBadgeCategory() {
        return badgeCategory;
    }

    public void setBadgeCategory(String badgeCategory) {
       this.badgeCategory = badgeCategory;
    }

    private String badgeCategory;

    public String getBadgeName() {
        return badgeName;
    }

    public void setBadgeName(String badgeName) {
        this.badgeName = badgeName;
    }

    private String badgeName;

    public String getBadgeText() {
        return badgeText;
    }

    public void setBadgeText(String badgeText) {
        this.badgeText = badgeText;
    }

    public boolean isFoundBadge() {
        return foundBadge;
    }

    public void setFoundBadge(boolean foundBadge) {
        this.foundBadge = foundBadge;
    }


    private String badgeUrlH;

    public String getBadgeUrlH() {
        return badgeUrlH;
    }

    public void setBadgeUrlH(String badgeUrlH) {
        this.badgeUrlH = badgeUrlH;
    }

    private String badgeUrlL;
    private int earnedBadge;
    private boolean foundBadge;
    private String badgeText;

    public String getBadgeImage() {
        return badgeImage;
    }

    public void setBadgeImage(String badgeImage) {
        this.badgeImage = badgeImage;
    }

    private String badgeImage;

    public String getBadgeUrlL() {
        return badgeUrlL;
    }

    public void setBadgeUrlL(String badgeUrlL) {
        this.badgeUrlL = badgeUrlL;
    }

    public int getEarnedBadge() {
        return earnedBadge;
    }

    public void setEarnedBadge(int earnedBadge) {
        this.earnedBadge = earnedBadge;
    }
}
