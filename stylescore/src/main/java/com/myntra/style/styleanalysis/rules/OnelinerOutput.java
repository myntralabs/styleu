package com.myntra.style.styleanalysis.rules;

public class OnelinerOutput  {

    public String getOneLinerText() {
        return oneLinerText;
    }

    public void setOneLinerText(String oneLinerText) {
        this.oneLinerText = oneLinerText;
    }
    private String oneLinerText;

    public String getOnelinercategory() {
        return onelinercategory;
    }

    public void setOnelinercategory(String onelinercategory) {
        this.onelinercategory = onelinercategory;
    }

    private String onelinercategory;
}
