package com.myntra.style.styleanalysis.impl;

public class StyleAnalysisPersistenceEntity {
    private String deviceId;
    private double overallScore;
    private String completeResponse;
    private String imageUrl;
    private String badgeCategory;
    private int    badgeId;

    public String getBadgeCategory() {
        return badgeCategory;
    }

    public void setBadgeCategory(String badgeCategory) {
        this.badgeCategory = badgeCategory;
    }

    public int getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCompleteResponse() {
        return completeResponse;
    }

    public void setCompleteResponse(String completeResponse) {
        this.completeResponse = completeResponse;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public double getOverallScore() {
        return overallScore;
    }

    public void setOverallScore(double overallScore) {
        this.overallScore = overallScore;
    }
}
