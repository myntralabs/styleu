create table onelinerconfig(
 id int NOT NULL AUTO_INCREMENT,
 category varchar(255) NOT NULL,
 onelinertext varchar(1000) NOT NULL,
 priority int,
 created_on datetime DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY(id)
);

insert into onelinerconfig(category,onelinertext,priority) values('Compliments','Are you a 90-degree angle? Cause you are looking right',1);
insert into onelinerconfig(category,onelinertext,priority) values ('Compliments','Wow, when God made you he was showing off.',2);
insert into onelinerconfig(category,onelinertext,priority) values ('Compliments',"You're even better than a unicorn, because you're real.",3);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"Dress like you are already famous",1);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"Life is short make everyoutfit count.",2);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"Life is a party ,dress like it",3);
insert into onelinerconfig(category,onelinertext,priority) values ('Insult',"This too shall pass, it might pass like a kidney stone though.",1);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"You are a good person but a lousy dresser",2);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"I dont have the energy to pretend to like you today.",3);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"When you looked in the mirror your reflection walked away.",4);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"Why dont you slip into something more confortable like a coma",5);
insert into onelinerconfig(category,onelinertext,priority) values ('Neutral',"You make onions cry",6);




create table onelinerrule(
 id int NOT NULL AUTO_INCREMENT,
 minscore decimal(6,3),
 maxscore decimal(6,3),
 onelinercategory varchar(255),
 created_on datetime DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY(id)
);


insert into onelinerrule(minscore,maxscore,onelinercategory) values(90,100,'Compliments');
insert into onelinerrule(minscore,maxscore,onelinercategory) values(80,90,'Compliments');
insert into onelinerrule(minscore,maxscore,onelinercategory) values(70,80,'Neutral');
insert into onelinerrule(minscore,maxscore,onelinercategory) values(60,70,'Neutral');
insert into onelinerrule(minscore,maxscore,onelinercategory) values(50,60,'Insult');