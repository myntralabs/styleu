create table scorerule(
 id int NOT NULL AUTO_INCREMENT,
 minscore decimal(6,3),
 maxscore decimal(6,3),
 scoretext varchar(255),
 created_on datetime DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY(id)
);

insert into scorerule(minscore,maxscore,scoretext) values(90,100,'90 Percentile');
insert into scorerule(minscore,maxscore,scoretext) values(80,90,'80 Percentile');
insert into scorerule(minscore,maxscore,scoretext) values(70,80,'70 Percentile');
insert into scorerule(minscore,maxscore,scoretext) values(60,70,'Let’s style up a bit more');
insert into scorerule(minscore,maxscore,scoretext) values(50,60,'We can surely do better');
insert into scorerule(minscore,maxscore,scoretext) values(0,50,'Ummm, we need to do some work here');
