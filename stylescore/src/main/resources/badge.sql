create table badge(
 id int NOT NULL AUTO_INCREMENT,
 badgecategory varchar(255),
 badgename varchar(255),
 badgetext varchar(255),
 badgeoneliner varchar(255),
 created_on datetime DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY(id)
);

CREATE INDEX idx_badge_badgecategory ON badge (badgencategory(255));
CREATE INDEX idx_badge_badgename ON badge(badgename(255));

insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Beginners Luck','Fashionista','Congratulations on winning your first Badge!','Stay Fashionable till the next time I see you.');
insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Beginners Luck','Stunner','You are on a roll.','You light up the room.');
insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Beginners Luck','Charmer','Keep up the stylish streak.','You look great today, just like every other day.');

insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Performance based','Majestic','You are in top 1% ','It’s gotta be illegal to look that good!');
insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Performance based','Smasher','You are in top 1%','Your mirror is way too lucky! Every time you look into it, it gets to look back at you');
insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Performance based','Splendid','You are in top 1%','Your mirror is way too lucky! Every time you look into it, it gets to look back at you');

insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Tenure Based','Style Icon','Keep up the stylish streak.','You look great today, just like every other day.');
insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Tenure Based','Fashion Monger','Celebrating your fashion journey with us','Stay Fashionable till the next time I see you.');
insert into badge(badgecategory,badgename,badgetext,badgeoneliner) values('Tenure Based','Head turner','Celebrating your fashion journey with us','Stay Fashionable till the next time I see you.');

create table badgerule (
id int NOT NULL AUTO_INCREMENT,
minvisitnumber int,
maxvisitnumber int,
visitminscore decimal(6,3),
visitmaxscore decimal(6,3),
minpercentile decimal(6,3),
maxpercentile decimal(6,3),
milestonescore decimal(6,3),
milestonepercentile decimal(6,3),
milestonevisitnumber int,
scorevisitmin int,
scorevisitmax int,
badgecategory varchar(255),
created_on datetime default CURRENT_TIMESTAMP,
PRIMARY KEY(id)
);

-- Beginners--
-- SELECT BADGECATEGORY FROM BADGERULE WHERE ? < MAXVISITNUMBER AND ? > VISITMINSCORE LIMIT 1;
INSERT INTO BADGERULE(maxvisitnumber,visitminscore,badgecategory) values(3,75,'Beginners Luck');

-- SELECT BADGECATEGORY FROM BADGERULE WHERE ? > MINPERCENTILE LIMIT 1;
-- SELECT BADGECATEGORY FROM BADGERULE WHERE ? > MINPERCENTILE AND ? > MILESTONEVISITNUMBER LIMIT 1;
--- SELECT BADGECATEGORY FROM BADGERULE WHERE ? VISITMINSCORE LIMIT 1;
INSERT INTO badgerule(minpercentile, badgecategory) values(99,'Performance Based');
INSERT INTO badgerule(minpercentile,milestonevisitnumber,badgecategory) values(95,3,'Performance Based');
INSERT INTO badgerule(visitminscore,badgecategory) values(95,'Performance Based');

--- select badgecategory from badgerule where ? > milestonevisitnumber and ? > milestonescore limit 1;
INSERT INTO badgerule(milestonevisitnumber,milestonescore,badgecategory) values(25,70,'Tenure Based');
INSERT INTO badgerule(milestonevisitnumber,milestonescore,badgecategory) values(50,70,'Tenure Based');
INSERT INTO badgerule(milestonevisitnumber,milestonescore,badgecategory) values(25,70,'Tenure Based');


